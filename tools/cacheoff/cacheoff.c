#include <linux/init.h>
#include <linux/module.h>
MODULE_LICENSE("Dual BSD/GPL");
static int cacheoff_init(void)
{
        printk(KERN_ALERT "Disabling L1 and L2 caches.\n");
        asm(
                "push   %eax\n\t"
                "movl   %cr0, %eax\n\t"
                "orl    $0x40000000, %eax\n\t"
                "movl   %eax, %cr0\n\t"
                "wbinvd\n\t"
                "pop    %eax\n\t"
        );
        return 0;
}
static void cacheoff_exit(void)
{
        printk(KERN_ALERT "Enabling L1 and L2 caches.\n");
        asm(
                "push   %eax\n\t"
                "movl   %cr0, %eax\n\t"
                "andl   $0xBFFFFFFF, %eax\n\t"
                "movl   %eax, %cr0\n\t"
                "wbinvd\n\t"
                "pop    %eax\n\t"
        );
}
module_init(cacheoff_init);
module_exit(cacheoff_exit);
