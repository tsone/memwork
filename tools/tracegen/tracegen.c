/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include <stdio.h>
#include <malloc.h>
#include <assert.h>
#include <math.h>


#define MODE_FRAG       1       /* Fragmentation test trace. */
#define MODE_SMALL      2       /* Small block test trace. */
#define MODE_WCET       3       /* WCET test trace. */
#define MODE            MODE_SMALL

#define SEED            0xF0DE1ABA

#define ASSERT(a)       assert(a)

#define TRACEF_NO_REMOVED       (-1)
#define TRACEF_FREE_MAGIC       0xFFFF
#define TRACEF_MAX_SLOTS        (TRACEF_FREE_MAGIC - 1)

#define TRACEF_SLOTS_ATOM       100

#if (MODE == MODE_FRAG)

#define HEAP_SIZE       (3 * 1024 * 1024)       /* =3MB */
#define OUTPUT_FN       "trace-frag.dat"

#elif (MODE == MODE_SMALL)

#define OUTPUT_FN       "trace-small.dat"

#endif

#define MIN_MALLOC      1
#define MAX_MALLOC      512

#define FREE_INTERVAL   1


typedef struct __attribute__ ((packed)) t_tracef_slot {

        unsigned short slot;
        unsigned short size;

} tracef_slot;


typedef struct t_tracef_slots {

        tracef_slot     *_buf;
        size_t          _top;
        size_t          _max;

} tracef_slots;


typedef struct t_tracef {

        /** Buffer of slots. */
        tracef_slots    _slots;
        /** Buffer of allocations and frees. */
        tracef_slots    _buf;
        /** Index of last removed or -1 if none. */
        ptrdiff_t       _last_removed;
        size_t          _heap_size;

} tracef;


static size_t s_buckets[MAX_MALLOC - MIN_MALLOC + 1];


static void tracef_slots_init(tracef_slots *p) {
        ASSERT(p);

        p->_max = TRACEF_SLOTS_ATOM;
        p->_buf = malloc(sizeof(tracef_slot) * p->_max);
        ASSERT(p->_buf);
        p->_top = 0;

}


static size_t tracef_slots_get_size(tracef_slots *p, size_t slot_idx) {
        ASSERT(p && p->_buf);
        ASSERT(slot_idx <= p->_top);
        const size_t result = p->_buf[slot_idx].size;
        ASSERT((result >= MIN_MALLOC) && (result <= MAX_MALLOC));

        return result;
}


static void tracef_slots_set(tracef_slots *p, size_t slot_idx, tracef_slot slot) {
        ASSERT(p && p->_buf);
        ASSERT(slot_idx <= p->_top);

        p->_buf[slot_idx] = slot;

}


static void tracef_slots_push(tracef_slots *p, tracef_slot slot) {
        ASSERT(p && p->_buf);
        ASSERT(p->_top < p->_max);
        ASSERT(slot.slot < TRACEF_MAX_SLOTS);

        tracef_slots_set(p, p->_top, slot);
        ++p->_top;

        if (p->_top == p->_max) {

                p->_max += TRACEF_SLOTS_ATOM;
                p->_buf = realloc(p->_buf, sizeof(tracef_slot) * p->_max);
                ASSERT(p->_buf);

        }

}


static void tracef_slots_free(tracef_slots *p) {
        ASSERT(p && p->_buf);

        free(p->_buf);
        p->_buf = 0;

}


static void tracef_init(tracef *p) {
        ASSERT(p);

        tracef_slots_init(&p->_slots);
        tracef_slots_init(&p->_buf);
        p->_last_removed = TRACEF_NO_REMOVED;
        p->_heap_size = 0;

}


static void tracef_malloc(tracef *p, size_t size_bytes) {
        ASSERT(p);
        ASSERT((size_bytes >= MIN_MALLOC) && (size_bytes <= MAX_MALLOC));
        tracef_slot s = { .size = size_bytes };

        if (TRACEF_NO_REMOVED == p->_last_removed) {

                s.slot = p->_slots._top;
                tracef_slots_push(&p->_slots, s);

        } else {

                s.slot = p->_last_removed;
                tracef_slots_set(&p->_slots, s.slot, s);
                p->_last_removed = TRACEF_NO_REMOVED;

        }

        tracef_slots_push(&p->_buf, s);

        p->_heap_size += size_bytes;

        ++s_buckets[size_bytes - MIN_MALLOC];

}


static void tracef_free(tracef *p, size_t slot_idx) {
        ASSERT(p);
        ASSERT(TRACEF_NO_REMOVED == p->_last_removed);
        const tracef_slot s = { .slot = slot_idx, .size = TRACEF_FREE_MAGIC };
        const size_t size_bytes = tracef_slots_get_size(&p->_slots, slot_idx);

        p->_last_removed = slot_idx;
        p->_heap_size -= size_bytes;

        tracef_slots_push(&p->_buf, s);

}


static void tracef_write(tracef *p, const char *fn) {
        ASSERT(p);
        ASSERT(fn);
        unsigned short header[3] = {
                        0xDEAD,         /* Random garbage. */
                        0xBEEF,         /* " */
                        p->_slots._top  /* Number of slots. */
        };

        FILE *f = fopen(fn, "wb");
        ASSERT(f);

        fwrite(header, 1, sizeof(header), f);

        if (p->_buf._top > 0) {

                fwrite(p->_buf._buf, 1, sizeof(tracef_slot) * p->_buf._top, f);

        } else {

                printf("WARNING: No allocation data written to output file %s.\n", fn);

        }

        fclose(f);

}


static void tracef_cleanup(tracef *p) {
        ASSERT(p);

        tracef_slots_free(&p->_slots);
        tracef_slots_free(&p->_buf);

}


static double buckets_variance() {
        size_t i;
        double n = 0;
        double mean = 0;
        double M2 = 0;

        for (i = 0; i < sizeof(s_buckets) / sizeof(*s_buckets); i++) {
                ++n;
                double delta = ((double) s_buckets[i]) - mean;
                mean += delta / n;
                M2 += delta * (((double) s_buckets[i]) - mean);
        }

        return M2 / (n - 1);
}


typedef struct t_cycle {

        size_t *_buf;
        size_t _len;
        size_t _idx;

} cycle;


static void _cycle_shuffle(cycle *p) {
        ASSERT(p && p->_buf);
        size_t i;
        size_t j;
        size_t tmp;

        /* Durstenfeld shuffle. */
        for (i = p->_len - 1; i > 0; i--) {

                j = rand() % (i + 1);
                tmp = p->_buf[i];
                p->_buf[i] = p->_buf[j];
                p->_buf[j] = tmp;

        }

}


static void cycle_init(cycle *p, size_t shuffle_count, size_t seed) {
        ASSERT(p);
        size_t i;

        srand(seed);

        p->_idx = 0;

#if (MODE == MODE_FRAG)
        p->_len = (MAX_MALLOC + 1 - MIN_MALLOC);
        p->_buf = malloc(sizeof(size_t) * p->_len);
        ASSERT(p->_buf);

        for (i = 0; i < p->_len; i++) {

                p->_buf[i] = i + MIN_MALLOC;

        }

#elif (MODE == MODE_SMALL)

        /* Calculate gaussian/normal distribution. */
        /* FIXME: Unstable! */
        size_t j, k = 0;
        size_t tot = 0;
        /*// 3MB
        const double mean = 48.0;
        const double stddev = 20.0;
        const double cutoff = 128.0f;
        */
        /*// 3MB
        const double mean = 44.0;
        const double stddev = 18.0;
        const double cutoff = 108.0f;
        */
        /*// 3MB
        const double mean = 32.0 + 4.0;
        const double stddev = 17.0;
        const double cutoff = 92.0f;
        */
        const double stddmul = 1.644854; /* 90% */
        const double mean = 32.0;
        const double stdd = 32.0 / stddmul;
        const double cutoff = 108.0f;

        double b = (cutoff - mean) / stdd;
        double a = 1.0 / (stdd * sqrt(2.0 * M_PI));
        double scaler = a * exp(-0.5 * b * b);
        a *= 1.0 / scaler;

        p->_len = 0;
        for (i = MIN_MALLOC; i <= MAX_MALLOC; i++) {

                b = (i - mean) / stdd;
                double y = a * exp(-0.5 * b * b);
                size_t n = ceil(y);//y >= 1.0 ? ceil(y) : 1;
                p->_len += n;

        }
        printf("INFO: Gaussian cycle length = %d\n", p->_len);
        ASSERT(p->_len < 1*1024*1024);

        p->_buf = malloc(sizeof(size_t) * p->_len);
        ASSERT(p->_buf);

        for (i = MIN_MALLOC; i <= MAX_MALLOC; i++) {

                b = (i - mean) / stdd;
                double y = a * exp(-0.5 * b * b);
                size_t n = ceil(y);// y >= 1.0 ? ceil(y) : 1;

                for (j = 0; j < n; j++) {

                        p->_buf[k] = i;
                        ++k;

                }

        }

#endif

        for (i = 0; i < shuffle_count; i++) {

                _cycle_shuffle(p);

        }

}


static size_t cycle_get(cycle *p) {
        ASSERT(p && p->_buf);
        size_t result;

        if (0 == p->_idx) {

                _cycle_shuffle(p);

        }

        result = p->_buf[p->_idx];

        if (p->_idx < p->_len - 1) {

                ++p->_idx;

        } else {

                p->_idx = 0;

        }

        return result;
}


static void cycle_cleanup(cycle *p) {
        ASSERT(p && p->_buf);

        free(p->_buf);
        p->_buf = 0;

}


static int generate() {
        tracef t;
        cycle c;
        size_t i;
        tracef_init(&t);

        for (i = 0; i < sizeof(s_buckets) / sizeof(*s_buckets); i++) {

                s_buckets[i] = 0;

        }

/*
        for (i = MIN_MALLOC; i <= MAX_MALLOC; i++) {

                tracef_malloc(&t, i);

        }
*/

        cycle_init(&c, 3, SEED);

#if (MODE == MODE_FRAG)

        /* Don't stop until heap is full.*/
        for (i = 0; t._heap_size < HEAP_SIZE; i++) {

#elif (MODE == MODE_SMALL)

        /* Don't stop until all items in cycle have been used. */
        for (i = 0; i < c._len; i++) {

#endif

                tracef_malloc(&t, cycle_get(&c));

                if (i & 1) {

                        tracef_free(&t, rand() % t._slots._top);

                }

        }

        cycle_cleanup(&c);

        printf("INFO: Heap size = %d.\n", t._heap_size);
        printf("INFO: Slots = %d.\n", t._slots._top);
        printf("INFO: Buffer items = %d.\n", t._buf._top);
        printf("INFO: Buckets variance = %.3f\n", buckets_variance());

/*
        printf("INFO: Buckets (%d):\n", sizeof(s_buckets) / sizeof(*s_buckets));
        for (i = 0; i < sizeof(s_buckets) / sizeof(*s_buckets); i++) {

                printf("%-5d %-5d\n", i + MIN_MALLOC, s_buckets[i]);

        }
*/

        tracef_write(&t, OUTPUT_FN);

        tracef_cleanup(&t);
        return 0;
}


int main(void) {
        int result = -1;

#if 0
        /* Calculate normal distribution for block sizes. */
        size_t i, j;
        size_t tot = 0;
        size_t a = 32;
        size_t b = MAX_MALLOC;
        double max = -5.5;
        double scaler = 1.0 / exp(max);
        for (i = a; i <= MAX_MALLOC; i++) {

                double x = (double) (i - a) / (double) (b - a);
                double e = scaler * exp(max * x);
                printf("%-6d%-5d%8.3f", i, (size_t) e, e);
                for (j = 0; j < (size_t) e; j++) {
                        printf("*");
                }
                puts("");
                tot += (size_t) e;

        }
        printf("INFO: Total = %d\n", tot);
#endif

        printf("Generating (MODE = %d)...\n", MODE);
        result = generate();
        puts("DONE!");

        return result;
}
