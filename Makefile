#---------------------------------------------------------------------------------
.SUFFIXES:
.PHONY: all clean cacheoff clean-cacheoff tracegen clean-tracegen
#---------------------------------------------------------------------------------

BASEDIR		?= $(CURDIR)
export BASEDIR

#---------------------------------------------------------------------------------
# Configuration is defined from variables: PLATFORM, BUILD.
# BUILDDIR is the directory where intermediate files will be placed.
#---------------------------------------------------------------------------------
PLATFORM	?= linux
ifeq ($(PLATFORM),nds)
else ifeq ($(PLATFORM),w32)
else ifeq ($(PLATFORM),linux)
else
$(error "Export PLATFORM = nds|w32|linux, default: linux")
endif

BUILD		?= debug
ifeq ($(BUILD),debug)
else ifeq ($(BUILD),release)
else ifeq ($(BUILD),test)
else
$(error "Export BUILD = debug|release|test, default: debug")
endif
BUILDDIR	:= $(PLATFORM)/$(BUILD)


#---------------------------------------------------------------------------------
# Setup sources and includes.
# - TARGET is the name of the output.
# - SOURCES is a list of directories containing source code.
# - INCLUDES is a list of directories containing extra header files.
#---------------------------------------------------------------------------------
TARGET		:= $(BUILDDIR)/memwork
SOURCES		:= gfx source/alloc source/fw data
INCLUDES	:= include include/alloc include/fw build
ifeq ($(BUILD),test)
SOURCES		+= source/test
INCLUDES	+= include/test
else
SOURCES		+= source
endif


#---------------------------------------------------------------------------------
# Any extra libraries we wish to link with the project.
#---------------------------------------------------------------------------------
LIBS		:= 


#---------------------------------------------------------------------------------
# list of directories containing libraries, this must be the top level containing
# include and lib.
#---------------------------------------------------------------------------------
LIBDIRS		:=


#---------------------------------------------------------------------------------
# Define debug, optimization and warning options from BUILD.
#---------------------------------------------------------------------------------
ifeq ($(BUILD),release)
DOPTS		:= -s -DNDEBUG

OOPTS		:= -O3 \
		-fomit-frame-pointer \
		-finline-functions \
		-fno-stack-protector \
		-ffast-math

else
DOPTS		:= -g -DDEBUG
OOPTS		:= -O0
endif
WOPTS		:= -Wpointer-arith -Wall -Wstack-protector 


#---------------------------------------------------------------------------------
# Collect compiler and linker options.
#---------------------------------------------------------------------------------
CFLAGS		:= $(DOPTS) $(OOPTS) $(WOPTS) $(INCLUDE)
ASFLAGS		:= $(DOPTS)
LDFLAGS		:= $(DOPTS) $(OOPTS)

# Finalize C and C++ options
CXXFLAGS 	:= -xc++ $(CFLAGS) -fno-rtti -fno-exceptions
CFLAGS		:= -xc $(CFLAGS)


#---------------------------------------------------------------------------------
# Include PLATFORM configuration.
#---------------------------------------------------------------------------------
include $(BASEDIR)/$(PLATFORM).mk


#---------------------------------------------------------------------------------
# No real need to edit anything past this point unless you need to add additional
# rules for different file extensions.
#---------------------------------------------------------------------------------
ifneq ($(BUILD),$(notdir $(CURDIR)))
#---------------------------------------------------------------------------------
 
export OUTPUT	:=	$(BASEDIR)/$(TARGET)
 
export VPATH	:=	$(foreach dir,$(SOURCES),$(BASEDIR)/$(dir))
export DEPSDIR	:=	$(BASEDIR)/$(BUILDDIR)

CFILES		:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.c)))
CPPFILES	:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.cpp)))
SFILES		:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.s)))
BINFILES	:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.bin)))
 
#---------------------------------------------------------------------------------
# use CXX for linking C++ projects, CC for standard C
#---------------------------------------------------------------------------------
ifeq ($(strip $(CPPFILES)),)
#---------------------------------------------------------------------------------
	export LD	:= $(CC)
#---------------------------------------------------------------------------------
else
#---------------------------------------------------------------------------------
	export LD	:= $(CXX)
#---------------------------------------------------------------------------------
endif
#---------------------------------------------------------------------------------

export OFILES	:= $(BINFILES:.bin=.o) \
					$(CPPFILES:.cpp=.o) $(CFILES:.c=.o) $(SFILES:.s=.o)
 
export INCLUDE	:= $(foreach dir,$(INCLUDES),-I$(BASEDIR)/$(dir)) \
					$(foreach dir,$(LIBDIRS),-I$(dir)/include) \
					-I$(BASEDIR)/$(BUILDDIR)
 
export LIBPATHS	:= $(foreach dir,$(LIBDIRS),-L$(dir)/lib)
 
.PHONY: $(BUILDDIR)

all: $(BUILDDIR)

#---------------------------------------------------------------------------------
$(BUILDDIR):
	@[ -d $@ ] || mkdir -p $@
	$(MAKE) --no-print-directory -C $(BUILDDIR) -f $(BASEDIR)/Makefile
 
#---------------------------------------------------------------------------------
clean:
	@echo clean ...
	@rm -fr $(PLATFORM)


#---------------------------------------------------------------------------------
# Forwards to submake
#---------------------------------------------------------------------------------
cacheoff:
	$(MAKE) --no-print-directory -C tools/cacheoff/

tracegen:
	$(MAKE) all --no-print-directory -C tools/tracegen/

clean-tracegen:
	$(MAKE) clean --no-print-directory -C tools/tracegen/
 
#---------------------------------------------------------------------------------
else


DEPENDS		:= $(OFILES:.o=.d)
OUTPUTBASE	:= $(basename $(OUTPUT))

all: $(OUTPUT)
 
#---------------------------------------------------------------------------------
# main targets
#---------------------------------------------------------------------------------
$(OUTPUTBASE).nds : $(OUTPUTBASE).elf
$(OUTPUTBASE).elf : $(OFILES)

$(OUTPUTBASE).exe : $(OFILES)
	@echo linking $(notdir $@)
	$(LD)  $(LDFLAGS) $(OFILES) $(LIBPATHS) $(LIBS) -o $@

$(OUTPUTBASE) : $(OFILES)
	@echo linking $(notdir $@)
	$(LD)  $(LDFLAGS) $(OFILES) $(LIBPATHS) $(LIBS) -o $@
 
#---------------------------------------------------------------------------------
%.o	:	%.bin
#---------------------------------------------------------------------------------
	@echo $(notdir $<)
	$(bin2o)
 
 
-include $(DEPENDS)
 
#---------------------------------------------------------------------------------------
endif
#---------------------------------------------------------------------------------------
