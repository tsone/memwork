/**
 * Pool implementation.
 *
 * Allocates fixed size blocks in constant-time, and has zero memory
 * overhead. Free blocks are stored in a single-linked list which
 * pointers are contained in the free blocks themselves.
 *
 */
#define POOL_H_
#ifndef FREE_LIST_H_
#include "free_list.h"
#endif
#ifndef REGION_H_
#include "region.h"
#endif
#ifndef ALLOCATOR_H_
#include "allocator.h"
#endif


typedef struct t_pool {

	/**
	 * Free list.
	 */
	free_list free;

	/**
	 * Region allocator for bumping pointer.
	 */
	region bump;

	/**
	 * Block size in bytes.
	 */
	size_t block_size_bytes;

#ifndef NDEBUG
	info info;
#endif

} pool;


void	pool_init(pool *a, size_t block_size_bytes, void *pool_start, void *pool_end);
void*	pool_alloc(pool *a);
void	pool_free(pool *a, void *p);
