#if 0
#include "pages.h"
#ifndef REAP_H_
#include "reap.h"
#endif
#ifndef INTERNAL_H_
#include "internal.h"
#endif


typedef struct t_pages {

	/**
	 * Page reap for allocating pages.
	 */
	reap page_reap;

	/**
	 * Page reap base address.
	 */
	void *base;

	/**
	 * Page size in bytes.
	 */
	size_t page_size_bytes;

} pages;


void pages_init(pages *p, size_t page_size_bytes, void *start, void *end) {
	REAP_INIT(p->page_reap, start, end)
	p->base = start;
	p->page_size_bytes = page_size_bytes;
}


void* pages_alloc(pages *p) {
	void *result;
	REAP_ALLOC_AUTO(p->page_reap, p->page_size_bytes, result)
	return result;
}


void pages_free(pages *p, void *ptr) {
	REAP_FREE_AUTO(p->page_reap, ptr, p->page_size_bytes)
}


#endif
