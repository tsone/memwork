#if 0
#include "pool.h"


void pool_init(pool *a, size_t block_size_bytes, void *pool_start, void *pool_end) {

	FREE_LIST_INIT(a->free);
//	a->free = 0;

	REGION_INIT(a->bump, pool_start, pool_end);
//	a->bump = area_start;
//	a->num_blocks = num_blocks;

	a->block_size_bytes = block_size_bytes;

#ifndef NDEBUG
	a->info.capacity_bytes = pool_end - pool_start;
	a->info.free_bytes = a->info.capacity_bytes;
	a->info.implementation_bytes = sizeof(*a);
#endif

}


void* pool_alloc(pool *a) {

	void *result;

	if (!FREE_LIST_IS_EMPTY(a->free)) {
//	if (a->free) {

		FREE_LIST_POP(a->free, result);
//		result = (void*) a->free;
//		a->free = (size_t*) *a->free;

#ifndef NDEBUG
		a->info.free_bytes -= a->block_size_bytes;
#endif

	} else if (!REGION_IS_FULL(a->bump)) {
//	} else if (a->num_blocks) {

//		result = (void*) a->bump;
//		--a->num_blocks;
//		a->bump += a->block_size_words;

		REGION_ALLOC_UNSAFE(a->bump, a->block_size_bytes, result);

#ifdef DEBUG
		a->info.free_bytes -= a->block_size_bytes;
#endif

	} else {

		result = 0;

	}

	return result;
}


void pool_free(pool *a, void *p) {

	FREE_LIST_PUSH(a->free, p);
//	*((size_t*) p) = (size_t) a->free;
//	a->free = (size_t*) p;

#ifndef NDEBUG
	a->info.free_bytes += a->block_size_bytes;
#endif

}

#endif
