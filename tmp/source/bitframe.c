#if 0
#include "bitframe.h"
#ifndef _STDIO_H_
#include <stdio.h>
#endif
#ifndef _MALLOC_H_
#include <malloc.h>
#endif


#define NUM_PAGES   256
#define TERMINATOR  (NUM_PAGES - 1)


typedef char i8;
typedef short i16;
typedef int i32;
typedef const char ci8;
typedef const short ci16;
typedef const int ci32;
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef const unsigned char cu8;
typedef const unsigned short cu16;
typedef const unsigned int cu32;

typedef struct t_bitframe {
	unsigned allocation : 8;
	unsigned lengths : 24;
} bitframe;

typedef struct t_link {
    u8 prev, next;
} link;

typedef struct t_page {
    bitframe    bitframes[NUM_PAGES];
    link        links[NUM_PAGES];
    
    int         heads[8];
    int         headbits;
    
    u8          *area;
    int         area_size;
    
    int         quantum_shift;
} page;


cu8 lookup_longest_idx[] = {0,1,2,2,3,3,3,3,4,4,4,4,4,4,4,4,0,1,5,5,5,5,5,5,0,5,5,5,5,5,5
,5,0,1,2,2,0,3,3,3,0,1,6,6,0,6,6,6,0,1,2,2,0,6,6,6,0,1,6,6,0,6,6,6,0,1,2,2,3,3,3
,3,0,1,4,4,0,4,4,4,0,1,2,2,0,1,0,3,0,1,0,2,0,1,0,5,0,1,2,2,0,3,3,3,0,1,0,2,0,1,0
,4,0,1,2,2,0,1,0,3,0,1,0,2,0,1,0,7,0,1,2,2,3,3,3,3,0,4,4,4,4,4,4,4,0,1,2,2,0,5,5
,5,0,1,5,5,0,5,5,5,0,1,2,2,0,3,3,3,0,1,0,2,0,1,0,4,0,1,2,2,0,1,0,3,0,1,0,2,0,1,0
,6,0,1,2,2,3,3,3,3,0,1,4,4,0,4,4,4,0,1,2,2,0,1,0,3,0,1,0,2,0,1,0,5,0,1,2,2,0,3,3
,3,0,1,0,2,0,1,0,4,0,1,2,2,0,1,0,3,0,1,0,2,0,1,0,0};

cu8 lookup_longest[] = {7,6,5,5,4,4,4,4,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2
,2,2,2,4,3,2,2,1,1,1,1,2,1,1,1,1,1,1,1,3,2,1,1,1,1,1,1,2,1,1,1,1,1,1,1,5,4,3,3,2
,2,2,2,2,1,1,1,1,1,1,1,3,2,1,1,1,0,0,0,2,1,0,0,1,0,0,0,4,3,2,2,1,1,1,1,2,1,0,0,1
,0,0,0,3,2,1,1,1,0,0,0,2,1,0,0,1,0,0,0,6,5,4,4,3,3,3,3,2,2,2,2,2,2,2,2,3,2,1,1,1
,1,1,1,2,1,1,1,1,1,1,1,4,3,2,2,1,1,1,1,2,1,0,0,1,0,0,0,3,2,1,1,1,0,0,0,2,1,0,0,1
,0,0,0,5,4,3,3,2,2,2,2,2,1,1,1,1,1,1,1,3,2,1,1,1,0,0,0,2,1,0,0,1,0,0,0,4,3,2,2,1
,1,1,1,2,1,0,0,1,0,0,0,3,2,1,1,1,0,0,0,2,1,0,0,1,0,0,0};


#define PAGE_DETACH_FRAME(p, idx, longest) \
    if (p->links[idx].prev != TERMINATOR) { \
        p->links[p->links[idx].prev].next = p->links[idx].next; \
    } \
    else { \
        p->heads[longest] = p->links[idx].next; \
        if (p->links[idx].next == TERMINATOR) { \
            p->headbits |= (1 << longest); \
        } \
    } \
    if (p->links[idx].next != TERMINATOR) { \
        p->links[p->links[idx].next].prev = p->links[idx].prev; \
    }

#define PAGE_ATTACH_FRAME(p, idx, new_longest) \
    if (p->heads[new_longest] != TERMINATOR) { \
        p->links[p->heads[new_longest]].prev = idx; \
    } \
    else { \
        p->headbits &= ~(1 << new_longest); \
    } \
    p->links[idx].next = p->heads[new_longest]; \
    p->links[idx].prev = TERMINATOR; \
    p->heads[new_longest] = idx;
    
    
void page_fix_list(page *p, int idx, int longest) {
    int new_longest = 8;
    if (p->bitframes[idx].allocation != 0xFF) {
        new_longest = lookup_longest[p->bitframes[idx].allocation];
    }
    if (longest != new_longest) {
        if (longest < 8) {
            PAGE_DETACH_FRAME(p, idx, longest);
        }
        if (new_longest < 8) {
            PAGE_ATTACH_FRAME(p, idx, new_longest);
        }
    }
}


void* page_alloc(page *p, int size_bytes) {
    /* locate suitable bitframe from heads list */
    int length = ((size_bytes - 1) >> p->quantum_shift);
    /* FIXME: if size_bytes < 1, then length is NEGATIVE, and that is BAD */
    /* FIXME: if size_bytes > (8 << p-block_size_shift), then length > 7, and it's BAD */
    int heads_idx = lookup_longest_idx[p->headbits | ((1 << length) - 1)];
    /* FIXME: what if lookup returns no free blocks? */
    int idx = p->heads[heads_idx];
    
    int longest = lookup_longest[p->bitframes[idx].allocation];
    
    /* set bitframe allocation and length bits */
    int bit_idx = lookup_longest_idx[p->bitframes[idx].allocation];
    p->bitframes[idx].allocation |= ((2 << length) - 1) << bit_idx;
    p->bitframes[idx].lengths |= length << (3 * bit_idx);
    
    /* if longest sequence changed, move bitframe to another heads list */
    page_fix_list(p, idx, longest);
    
    /* compute block pointer and return */
    /*return (void*) (p->area + ((bit_idx + (idx << 3)) << p->block_size_shift));*/
    u8* ptr = p->area + ((bit_idx + (idx << 3)) << p->quantum_shift);
    printf("alloc: %08X idx=%d bit_idx=%d length=%d longest=%d headbits=%02X heads_idx=%d\n",
        (size_t) ptr, idx, bit_idx, length, longest, p->headbits, heads_idx);
    return ptr;
}

void page_free(page *p, void *ptr) {
    int longest = 8;
    
    /* compute bitframe idx and bit_idx from ptr */
    int idx = (((u8*) ptr) - p->area) >> p->quantum_shift;
    int bit_idx = idx & 7;
    idx = idx >> 3;
    
    if (p->bitframes[idx].allocation != 0xFF) {
        longest = lookup_longest[p->bitframes[idx].allocation];
    }

    /* unset bitframe allocation */
    int length = (p->bitframes[idx].lengths >> (3 * bit_idx)) & 7;
    p->bitframes[idx].allocation &= ~(((2 << length) - 1) << bit_idx);
    /* length needs no modification */
    /* p->bitframes[idx].lengths &= ~(7 << (3 * bit_idx)); */

    /* if longest sequence changed, move bitframe to another heads list */
    page_fix_list(p, idx, longest);
    
    printf("free: %08X idx=%d bit_idx=%d length=%d longest=%d headbits=%02X\n",
        (size_t) ptr, idx, bit_idx, length, longest, p->headbits);
}

void page_init(page *p, int shift, void *area, u32 area_size) {
    int i;
    
    /* init area */
    p->area = (u8*) area;
    p->area_size = area_size;
    p->quantum_shift = shift;
    
    /* init heads */
    p->headbits = 0x7F; /* all have initially 8 free blocks */
    for (i = 0; i < 7; i++) {
        p->heads[i] = TERMINATOR;
    }
    p->heads[7] = 0;
    
    /* init bitframes */
    for (i = 0; i < NUM_PAGES; i++) {
        p->bitframes[i].allocation = 0;
        p->bitframes[i].lengths = 0;
    }
    
    /* init links */
    for (i = 0; i < NUM_PAGES; i++) {
        p->links[i].prev = i - 1;
        p->links[i].next = i + 1;
    }
    p->links[0].prev = TERMINATOR;
    p->links[NUM_PAGES - 1].next = TERMINATOR;
}
/*
int main(int argc, char* argv[]) {
    int i;
    void **q = malloc(4 * 100);
    page p;
    int area_size = 32*1024;
    void *area = malloc(area_size);
    
    page_init(&p, 2, area, area_size);
    for (i = 0; i < 16; i++) {
        *(q++) = page_alloc(&p, 4);
    }
    for (i = 0; i < 16; i++) {
        page_free(&p, *(--q));
    }
	
	free(area);
	return 0;
}
*/

#endif
