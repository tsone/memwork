/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#ifdef ARM9
#include <fat.h>
#endif
#ifndef TRACE_H_
#include "trace.h"
#endif
#ifndef FW_H_
#include "fw.h"
#endif
#ifndef INTERNAL_H_
#include "internal.h"
#endif
#ifndef _STDIO_H_
#include <stdio.h>
#endif
#ifndef _MALLOC_H_
#include <malloc.h>
#endif
#ifndef REGIS1_H_
#include "regis1.h"
#endif
#ifndef REGIS2_H_
#include "regis2.h"
#endif
#ifndef REAPS_H_
#include "reaps.h"
#endif
#ifndef BBUDDY_H_
#include "bbuddy.h"
#endif
#ifndef SSS_H_
#include "sss.h"
#endif
#ifndef BFRAME_H_
#include "bframe.h"
#endif
#ifndef TLSF_H_
#include "tlsf.h"
#endif


#define TRACE_NAME      "stable"


static void print_heap() {
#ifdef ARM9
//	iprintf("base: %07X\n", (size_t) getHeapStart());
	PRINT("%07X - %07X = %uK\n",
			(size_t) getHeapEnd(), (size_t) getHeapLimit(),
			((ptrdiff_t) getHeapLimit() - (ptrdiff_t) getHeapEnd()) / 1024);
#endif
}


#define ALLOCATOR(n_, init_args_, cleanup_args_) \
static size_t w_##n_##_init(void *start, size_t size) { return n_##_init init_args_; } \
static void* w_##n_##_malloc(size_t size) { return n_##_malloc(size); } \
static void	 w_##n_##_free(void *ptr) { n_##_free(ptr); } \
static void  w_##n_##_cleanup(void *start) { n_##_cleanup cleanup_args_; }
#include "allocator_list.h"
#undef ALLOCATOR


#define ALLOCATOR(name_, a_, b_) \
		{ #name_, \
		w_ ## name_ ## _init, \
		w_ ## name_ ## _malloc, \
		w_ ## name_ ## _free, \
		w_ ## name_ ## _cleanup },
static fw_allocator s_allocators[] = {
#include "allocator_list.h"
};
#undef ALLOCATOR


#ifdef ARM9

static const char sc_output_path[] = "emu/";

#else

#ifdef W32
static const char sc_output_path[] = "tools\\data-" TRACE_NAME "\\";
#elif LINUX
static const char sc_output_path[] = "tools/data-" TRACE_NAME "/";
#else
static const char sc_output_path[] = "hw/";
#endif

#endif

const char sc_input_trace[] = "trace-" TRACE_NAME ".dat";


static void doit() {
	size_t i;
	void *heap_start;
	size_t heap_size;
	size_t result;
        fw_generator g = { 0, 0 };

	heap_size = FW_TEST_HEAP_SIZE + FW_TEST_PAGE_SIZE;
	heap_start = malloc(heap_size);
	ASSERT(heap_start);

        trace_init(&g, sc_input_trace);

	for (i = 0; i < sizeof(s_allocators) / sizeof(*s_allocators); i++) {

		result = fw_init(&s_allocators[i], &g, sc_output_path);
		ASSERT(result);

		PRINT("Testing: %s\n", s_allocators[i].name);

		if (!fw_run(heap_start, heap_size)) {

		        PERROR("Full trace not run. Heap exhausted.\n");

		}

		fw_cleanup();

	}

        trace_cleanup();

	free(heap_start);

}


int main(void) {

#ifdef ARM9

	consoleDemoInit();

	if (!fatInit(4, true)) {
		PERROR("fatInitDefault failure: terminating\n");
		return 2;
	}

	PRINT("\x1b[32m- Memwork -\x1b[39m\n");

#elif W32

	SetProcessPriorityBoost(GetCurrentProcess(), TRUE);
	SetThreadPriorityBoost(GetCurrentThread(), TRUE);

	/* Raise priority to real-time!!! */
#if 0
	DWORD process_mask = 0;
	DWORD system_mask = 0;
	GetProcessAffinityMask(GetCurrentProcess(), &process_mask, &system_mask);
	PRINT("%08X %08X\n", (u32) process_mask, (u32) system_mask);
	process_mask &= 0x08;
	SetProcessAffinityMask(GetCurrentProcess(), process_mask);
	SetThreadAffinityMask(GetCurrentThread(), process_mask);
/*	PRINT("%08X\n", GetThreadAff0inityMask(GetCurrentThread()));*/

	PRINT("%08X\n", (u32) GetPriorityClass(GetCurrentProcess()));
	if (!SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS)) {
		PERROR("SetPriorityClass() failed");
	}
	PRINT("%08X\n", (u32) GetPriorityClass(GetCurrentProcess()));

	PRINT("%08X\n", GetThreadPriority(GetCurrentThread()));
	if (!SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL)) {
		PERROR("SetThreadPriority() failed");
	}
	PRINT("%08X\n", GetThreadPriority(GetCurrentThread()));
#endif

#endif

	PRINT("- Memwork -\n");

	print_heap();

	doit();

	print_heap();
	PRINT("DONE!");

#ifdef ARM9
	while(1) {
		swiWaitForVBlank();
	}
#endif

	return 0;
}

