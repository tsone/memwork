#include "test_main.h"


void test_list() {
	list p;
	size_t d1 = 0xF315ABBA;
	size_t d2 = 0x5EAF00D5;
	size_t *ptr = 0;

	LIST_INIT(p);
	ASSERT(LIST_IS_EMPTY(p));

	LIST_PUSH(p, &d1);
	ASSERT(p == &d1);
	LIST_PUSH(p, &d2);
	ASSERT(p == &d2);

	LIST_POP(p, ptr);
	ASSERT((ptr == &d2) && (*ptr == d2));
	LIST_POP(p, ptr);
	ASSERT((ptr == &d1) && (*ptr == d1));
	LIST_POP(p, ptr);
	ASSERT(ptr == 0);

}
