#include "test_main.h"


static void test_vlq_decode_sub(u32 n, unsigned char *buf) {
	vlq_buf vlqb = { buf, 0 };
	vlq_set(&vlqb);
	ASSERT(n == vlq_decode());
}


static void test_vlq_decode() {
	unsigned char x00000000[] = {
			0x00,
			0xDE, 0xAD, 0xBE, 0xEF	/* 4 bytes of extra garbage */
	};
	unsigned char x0000007E[] = {
			0x00 | ((0x0000007E >> 0) & 0x7F)
	};
	unsigned char x00000100[] = {
			0x80 | ((0x00000100 >> 0) & 0x7F),
			0x00 | ((0x00000100 >> 7) & 0x7F),
			0xAD, 0xBE, 0xEF		/* 3 bytes of extra garbage */
	};
	unsigned char x0000FCFC[] = {
			0x80 | ((0x0000FCFC >> 0) & 0x7F),
			0x80 | ((0x0000FCFC >> 7) & 0x7F),
			0x00 | ((0x0000FCFC >> 14) & 0x7F),
			0xBE, 0xEF				/* 2 bytes of extra garbage */
	};
	unsigned char x00ABCDEF[] = {
			0x80 | ((0x00ABCDEF >> 0) & 0x7F),
			0x80 | ((0x00ABCDEF >> 7) & 0x7F),
			0x80 | ((0x00ABCDEF >> 14) & 0x7F),
			0x00 | ((0x00ABCDEF >> 21) & 0x7F),
			0xEF					/* 1 byte of extra garbage */
	};

	test_vlq_decode_sub(0x00000000, x00000000);
	test_vlq_decode_sub(0x0000007E, x0000007E);
	test_vlq_decode_sub(0x00000100, x00000100);
	test_vlq_decode_sub(0x0000FCFC, x0000FCFC);
	test_vlq_decode_sub(0x00ABCDEF, x00ABCDEF);

}


static void test_vlq_encode_sub(u32 n) {
	unsigned char buf[4];
	vlq_buf vlqb = { buf, 0 };

	vlq_set(&vlqb);
	vlq_encode(n);
	vlqb.idx = 0;
	ASSERT(n == vlq_decode());
}


static void test_vlq_encode() {

	test_vlq_encode_sub(0x00000000);
	test_vlq_encode_sub(0x0000003D);
	test_vlq_encode_sub(0x00000FED);
	test_vlq_encode_sub(0x0000FEED);
	test_vlq_encode_sub(0x00ABBAC0);
	test_vlq_encode_sub(0x0FFFFFFF);

}


static void test_vlq_decode_signed_sub(s32 n, unsigned char *buf) {
	vlq_buf vlqb = { buf, 0 };
	vlq_set(&vlqb);
	ASSERT(n == vlq_decode_signed());
}


static void test_vlq_decode_signed() {
	unsigned char p00000000[] = {
			0x00
	};
	unsigned char n00000000[] = {
			0x40 | 0x00
	};
	unsigned char p0000001F[] = {
			0x1F
	};
	unsigned char n0000001F[] = {
			0x40 | 0x1F
	};
	unsigned char p000000FF[] = {
			0x80 | 0x3F, 0x03
	};
	unsigned char n000000FF[] = {
			0x80 | 0x40 | 0x3F, 0x03
	};
	unsigned char p0000FFFF[] = {
			0x80 | 0x3F, 0x80 | 0x7F, 0x07
	};
	unsigned char n0000FFFF[] = {
			0x80 | 0x40 | 0x3F, 0x80 | 0x7F, 0x07
	};
	unsigned char p00FFFFFF[] = {
			0x80 | 0x3F, 0x80 | 0x7F, 0x80 | 0x7F, 0x0F
	};
	unsigned char n00FFFFFF[] = {
			0x80 | 0x40 | 0x3F, 0x80 | 0x7F, 0x80 | 0x7F, 0x0F
	};

	// The worm sign '~' is used because I'm lazy..
	// Ex. signed vlq 0x40 equals -1, i.e. ~0 in unsigned..
	test_vlq_decode_signed_sub(0x00000000, p00000000);
	test_vlq_decode_signed_sub(~0x00000000, n00000000);
	test_vlq_decode_signed_sub(0x0000001F, p0000001F);
	test_vlq_decode_signed_sub(~0x0000001F, n0000001F);
	test_vlq_decode_signed_sub(0x000000FF, p000000FF);
	test_vlq_decode_signed_sub(~0x000000FF, n000000FF);
	test_vlq_decode_signed_sub(0x0000FFFF, p0000FFFF);
	test_vlq_decode_signed_sub(~0x0000FFFF, n0000FFFF);
	test_vlq_decode_signed_sub(0x00FFFFFF, p00FFFFFF);
	test_vlq_decode_signed_sub(~0x00FFFFFF, n00FFFFFF);

}


static void test_vlq_encode_signed_sub(s32 n) {
	unsigned char buf[4];
	vlq_buf vlqb = { buf, 0 };

	vlq_set(&vlqb);
	vlq_encode_signed(n);
	vlqb.idx = 0;
	s32 result = vlq_decode_signed();
	ASSERT(n == result);
}


static void test_vlq_encode_signed() {

	test_vlq_encode_signed_sub(0x00000000);
	test_vlq_encode_signed_sub(0x0000001F);
	test_vlq_encode_signed_sub(-0x00000023);
	test_vlq_encode_signed_sub(0x000001FF);
	test_vlq_encode_signed_sub(-0x00000210);
	test_vlq_encode_signed_sub(0x00003456);
	test_vlq_encode_signed_sub(-0x00006543);
	test_vlq_encode_signed_sub(0x00102030);
	test_vlq_encode_signed_sub(-0x00765432);
	test_vlq_encode_signed_sub(0x07FFFFFF);
	test_vlq_encode_signed_sub(-0x07FFFFFF);

}

void test_vlq() {

	test_vlq_decode();
	test_vlq_encode();
	test_vlq_decode_signed();
	test_vlq_encode_signed();

}
