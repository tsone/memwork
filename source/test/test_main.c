/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include "test_main.h"


#define TEST(name_) extern void test_ ## name_ (void);

#include "test_list.h"

#undef TEST


int main(int argc, char *argv[]) {

#ifdef ARM9
	consoleDemoInit();
	PRINT("\x1b[32m- Memwork -\x1b[39m\n");
#else 
	PRINT("- Memwork -\n");
#endif
	PRINT("Running unit tests..\n\n");

#define TEST(name_) \
	PRINT("test_" #name_); \
	test_ ## name_ (); \
	PRINT("...OK\n");

#include "test_list.h"

#undef TEST

	PRINT("\nDONE!");

#ifdef ARM9
	while (1) {
		swiWaitForVBlank();
	}
#endif

	return 1;
}
