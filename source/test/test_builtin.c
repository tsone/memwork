#include "test_main.h"


#ifndef ARM9

/* NOTE: Test TLSF function replacements w/ built-ins. */
static const int table[] = {
    -1, 0, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4,
    4, 4,
    4, 4, 4, 4, 4, 4, 4,
    5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5, 5,
    5,
    5, 5, 5, 5, 5, 5, 5,
    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
    6,
    6, 6, 6, 6, 6, 6, 6,
    6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6, 6,
    6,
    6, 6, 6, 6, 6, 6, 6,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7,
    7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7,
    7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7,
    7, 7, 7, 7, 7, 7, 7,
    7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7, 7,
    7,
    7, 7, 7, 7, 7, 7, 7
};


static __inline__ int ls_bit(int i)
{
    unsigned int a;
    unsigned int x = i & -i;

    a = x <= 0xffff ? (x <= 0xff ? 0 : 8) : (x <= 0xffffff ? 16 : 24);
    return table[x >> a] + a;
}


static __inline__ int ls_bit_new(int i)
{
    return __builtin_ffs((unsigned int) i) - 1;
}


static __inline__ int ms_bit(int i)
{
    unsigned int a;
    unsigned int x = (unsigned int) i;

    a = x <= 0xffff ? (x <= 0xff ? 0 : 8) : (x <= 0xffffff ? 16 : 24);
    return table[x >> a] + a;
}


static __inline__ int ms_bit_new(int i)
{
    if (0 == i) {

    	return -1;
    } else {

    	/* NOTE: Assume 32-bit unsigned int. */
        return 31 - __builtin_clz((unsigned int) i);
    }
}


void test_builtin() {

	ASSERT(ms_bit(0x00000000) == -1);
	ASSERT(ms_bit(0x00000000) == ms_bit_new(0x00000000));
	ASSERT(ms_bit(0x0000000B) == ms_bit_new(0x0000000B));
	ASSERT(ms_bit(0x00008001) == ms_bit_new(0x00008001));
	ASSERT(ms_bit(0x07701301) == ms_bit_new(0x07701301));

	ASSERT(ls_bit(0x00000000) == -1);
	ASSERT(ls_bit(0x00000000) == ls_bit_new(0x00000000));
	ASSERT(ls_bit(0x0000000B) == ls_bit_new(0x0000000B));
	ASSERT(ls_bit(0x00008001) == ls_bit_new(0x00008001));
	ASSERT(ls_bit(0x07701301) == ls_bit_new(0x07701301));

}


#else


void test_builtin() {

	/* Do nothing. */

}


#endif
