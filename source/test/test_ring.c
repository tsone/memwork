#include "test_main.h"
#include <string.h>


typedef struct t_contact {

    const char *name;
    int id;
    cdl_node family;
    cdl_node friends;

} contact;


#define CONTACT(name_, id_) \
	contact name_ = { # name_, id_ }; \
	CDL_INIT(name_.family); \
	ASSERT(CDL_PREV(name_.family) == CDL_NEXT(name_.family)); \
	CDL_INIT(name_.friends); \
	ASSERT(CDL_PREV(name_.friends) == CDL_NEXT(name_.friends)); \


void test_ring() {
	CONTACT(jane, 1);
	CONTACT(john, 2);
	CONTACT(jack, 3);

	CDL_INSERT_AFTER(jane.family, john.family);
	ASSERT(CDL_NEXT(jane.family) == &john.family);
	ASSERT(CDL_PREV(john.family) == &jane.family);

	ASSERT(!CDL_IS_ORPHAN(jane.family));
	ASSERT(!CDL_IS_ORPHAN(john.family));

	CDL_INSERT_AFTER(jane.family, jack.family);
	ASSERT(CDL_NEXT(jack.family) == &john.family);
	ASSERT(CDL_PREV(jack.family) == &jane.family);
	ASSERT(CDL_NEXT(jane.family) == &jack.family);
	ASSERT(CDL_PREV(jane.family) == &john.family);
	ASSERT(CDL_NEXT(john.family) == &jane.family);
	ASSERT(CDL_PREV(john.family) == &jack.family);

	CDL_REMOVE(jack.family);
	ASSERT(CDL_IS_ORPHAN(jack.family));
	ASSERT(CDL_NEXT(jane.family) == &john.family);
	ASSERT(CDL_PREV(jane.family) == &john.family);
	ASSERT(CDL_NEXT(john.family) == &jane.family);
	ASSERT(CDL_PREV(john.family) == &jane.family);

	ASSERT(CDL_GET_BASE(jane.family, contact, family)->id == 1);
	ASSERT(CDL_GET_BASE(john.family, contact, family)->id == 2);
	ASSERT(CDL_GET_BASE(jack.family, contact, family)->id == 3);

	ASSERT(CDL_GET_BASE(jane.friends, contact, friends)->id == 1);
	ASSERT(CDL_GET_BASE(john.friends, contact, friends)->id == 2);
	ASSERT(CDL_GET_BASE(jack.friends, contact, friends)->id == 3);

	ASSERT(!strcmp(CDL_GET_BASE(jane.family, contact, family)->name, "jane"));
	ASSERT(!strcmp(CDL_GET_BASE(john.family, contact, family)->name, "john"));
	ASSERT(!strcmp(CDL_GET_BASE(jack.family, contact, family)->name, "jack"));


}
