/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include "fw.h"
#ifndef INTERNAL_H_
#include "internal.h"
#endif
#ifndef TRACE_H_
#include "trace.h"
#endif
#ifndef SLOTS_H_
#include "slots.h"
#endif
#ifndef CYCLES_H_
#include "cycles.h"
#endif
#ifndef STAT_H_
#include "stat.h"
#endif
#ifndef CACHEF_H_
#include "cachef.h"
#endif
#ifndef _STDIO_H_
#include <stdio.h>
#endif
#ifndef _STRING_H_
#include <string.h>
#endif
#ifndef _MALLOC_H_
#include <malloc.h>
#endif
#if (LINUX && NDEBUG)
#include <sys/mman.h>
#include <sys/io.h>
#include <sched.h>
#endif


#define FW_SLOT_INVALID_SIZE ((void*) (-1))

#ifdef ARM9
#define OUTFILE_CACHE_SIZE		(2*1024)
#else
/* Set large enough to prevent disk write for timing tests. */
#define OUTFILE_CACHE_SIZE		(16*1024)
#endif


typedef struct t_fw_cycles_out {

#if (INFO_DISABLE == 1)

#ifndef ARM9
	size_t	op_index;

	size_t	*min_cycles;
#endif

	cachef	cache;
#endif

} fw_cycles_out;


typedef struct t_fw {

	fw_allocator	*allocator;
	void			*heap_start;

	size_t	iteration;

#if (INFO_DISABLE == 0)

	cachef	cache_stat;
	INFO(, prev_stat);

#else

#ifndef ARM9
	size_t	num_ops;
#endif

#endif

	fw_cycles_out malloc_out;
	fw_cycles_out free_out;

	const char *output_path;

} fw;


static fw s_fw;


static void fw_write_stat() {

#if (INFO_DISABLE == 0)

	cachef_write_vlq_signed(&s_fw.cache_stat, STAT_INFO._internal - s_fw.prev_stat._internal);
	cachef_write_vlq_signed(&s_fw.cache_stat, STAT_INFO._implementation - s_fw.prev_stat._implementation);
	cachef_write_vlq_signed(&s_fw.cache_stat, STAT_INFO._allocated - s_fw.prev_stat._allocated);
	cachef_write_vlq_signed(&s_fw.cache_stat,
			((ptrdiff_t) STAT_INFO._max_address - (ptrdiff_t) STAT_INFO._min_address)
			- ((ptrdiff_t) s_fw.prev_stat._max_address - (ptrdiff_t) s_fw.prev_stat._min_address));

	STAT_COPY(s_fw.prev_stat);

#endif

}


static void fw_write_cycles(fw_cycles_out *p, size_t cycles) {

#if (INFO_DISABLE == 1)


#ifdef NDS


	cachef_write_vlq(&p->cache, cycles);


#else
	ASSERT(p->op_index < s_fw.num_ops);

	if (p->min_cycles[p->op_index] >= cycles) {

		p->min_cycles[p->op_index] = cycles;

	}

	if (0 == s_fw.iteration) {

		cachef_write_vlq(&p->cache, p->min_cycles[p->op_index]);

	}

	++p->op_index;


#endif


#endif

	fw_write_stat();

}


static void open_outfile(cachef *p, const char *postfix) {
	ASSERT(s_fw.allocator && s_fw.allocator->name);
	char fn[128];

	strcpy(fn, s_fw.output_path);
	strcat(fn, s_fw.allocator->name);
	strcat(fn, postfix);

#ifndef NDEBUG
	size_t result = cachef_init(p, OUTFILE_CACHE_SIZE, fn, CACHEF_MODE_WRITE);
	ASSERT(result);
#else
	cachef_init(p, OUTFILE_CACHE_SIZE, fn, CACHEF_MODE_WRITE);
#endif

}


size_t fw_init(fw_allocator *allocator, fw_generator *generator, const char *output_path) {
	ASSERT(allocator && generator && output_path);
	ASSERT(generator->num_slots > 0);
	ASSERT(generator->num_ops > 0);

        s_fw.output_path = output_path;
	s_fw.allocator = allocator;

#if (INFO_DISABLE == 0)

	open_outfile(&s_fw.cache_stat, "s.dat");

#else

	open_outfile(&s_fw.malloc_out.cache, "m.dat");
	open_outfile(&s_fw.free_out.cache, "f.dat");

#ifndef ARM9
	s_fw.num_ops = generator->num_ops;
	const size_t min_cycles_size = sizeof(size_t) * s_fw.num_ops;
	ASSERT(s_fw.num_ops > 0);

	s_fw.malloc_out.min_cycles = malloc(min_cycles_size);
	memset(s_fw.malloc_out.min_cycles, 0xFFFFFFFF, min_cycles_size);
	s_fw.free_out.min_cycles = malloc(min_cycles_size);
	memset(s_fw.free_out.min_cycles, 0xFFFFFFFF, min_cycles_size);

#endif

#endif

	if (!slots_init(generator->num_slots)) {
		ASSERT(0);

		return 0;
	}

	return 1;
}


static size_t fw_run_from_trace() {
        trace_op op;

        while (trace_next(&op)) {

                if (op.size == TRACE_FREE_MAGIC) {

                        fw_free_slot(op.slot);

                } else {

                        if (!fw_malloc_slot(op.slot, op.size)) {
                                /* Malloc failed. Heap is full. */

                                return 0;
                        }

                }

        }

        return 1;
}


size_t fw_run(void *heap_start, size_t heap_size) {
	ASSERT(heap_start && (heap_size > 0));
	ASSERT(s_fw.allocator && s_fw.allocator->f_init && s_fw.allocator->f_cleanup);
	size_t run_ok = 1;

	s_fw.iteration = (FW_ITERATIONS >= 1) ? FW_ITERATIONS : 1;
	s_fw.heap_start = heap_start;

#if (LINUX && NDEBUG)
        if (iopl(3)) {
                PERROR("Skipped test. Unable to iopl(3). NOTE: Use sudo.\n");
                return 0;
        }
        if (mlockall(MCL_CURRENT | MCL_FUTURE)) {
                PERROR("Skipped test. Unable to mlockall(). NOTE: Use sudo.\n");
                iopl(0);
                return 0;
        }
        int maxp = sched_get_priority_max(SCHED_FIFO);
        //int minp = sched_get_priority_min(SCHED_FIFO);
        struct sched_param maxpp = {
                .sched_priority = maxp
        };
#endif

	do {

/*		PRINT("Iteration: %d\n", s_fw.iteration);*/

		--s_fw.iteration;

#ifdef NDEBUG

#ifndef ARM9
		s_fw.malloc_out.op_index = 0;
		s_fw.free_out.op_index = 0;
#endif

#ifdef LINUX

                if (sched_setscheduler(0, SCHED_FIFO, &maxpp)) {
                        PRINT("sched_setscheduler() failed.\n");
                }

#endif

#endif
		trace_rewind();

		INFO_INIT(s_fw.prev_stat, 0, 0);

		CYCLES_INIT();

		if (s_fw.allocator->f_init(heap_start, heap_size)) {

		        run_ok &= fw_run_from_trace();

                        s_fw.allocator->f_cleanup(s_fw.heap_start);

		} else {

			PERROR("Failed to initialize allocator %s.\n", s_fw.allocator->name);
			break;

		}

	} while (s_fw.iteration);

#if (LINUX && NDEBUG)
        munlockall();
        iopl(0);
#endif

        return run_ok;
}


void fw_cleanup() {

#ifndef NDEBUG

	cachef_cleanup(&s_fw.cache_stat);

#else

	cachef_cleanup(&s_fw.malloc_out.cache);
	cachef_cleanup(&s_fw.free_out.cache);

#ifndef ARM9

	free(s_fw.malloc_out.min_cycles);
	free(s_fw.free_out.min_cycles);

#endif

#endif

	slots_cleanup();

}


void fw_free_slot(size_t slot_idx) {
	ASSERT(s_fw.allocator && s_fw.allocator->f_free);
	void *ptr = slots_get(slot_idx);
	volatile size_t cycles = 0;

	if (ptr == FW_SLOT_INVALID_SIZE) {

		return;
	}

	INFO_HDR_ASSERT(ptr);
	STAT_ADD_ALLOCATED(-INFO_HDR_GET_ALLOCATED(ptr));
	STAT_ADD_INTERNAL(-INFO_HDR_GET_INTERNAL(ptr));
	STAT_ADD_IMPLEMENTATION(-INFO_HDR_GET_IMPLEMENTATION(ptr));
	INFO_HDR_CLEANUP(ptr);

	CYCLES_RESTART();
	s_fw.allocator->f_free(ptr);
	CYCLES_GET(cycles);

	fw_write_cycles(&s_fw.free_out, cycles);

}


size_t fw_malloc_slot(size_t slot_idx, size_t size_bytes) {
	ASSERT(s_fw.allocator && s_fw.allocator->f_malloc);
	void *ptr = 0;
	volatile size_t cycles = 0;

	if ((size_bytes >= FW_TEST_MIN_MALLOC) && (size_bytes <= FW_TEST_MAX_MALLOC)) {

		CYCLES_RESTART();
		ptr = s_fw.allocator->f_malloc(size_bytes);
		CYCLES_GET(cycles);

		if (!ptr) {
		        /* Failed to allocate. Heap probably exhausted. */

		        return 0;
		}

		INFO_HDR_ASSERT(ptr);
		STAT_ADD_ALLOCATED(INFO_HDR_GET_ALLOCATED(ptr));
		STAT_ADD_INTERNAL(INFO_HDR_GET_INTERNAL(ptr));
		STAT_ADD_IMPLEMENTATION(INFO_HDR_GET_IMPLEMENTATION(ptr));

		slots_set(slot_idx, ptr);

		fw_write_cycles(&s_fw.malloc_out, cycles);

	} else {

		slots_set(slot_idx, FW_SLOT_INVALID_SIZE);

	}

	return 1;
}

