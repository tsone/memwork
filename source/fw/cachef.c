/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include "cachef.h"
#ifndef INTERNAL_H_
#include "internal.h"
#endif
#ifndef VLQ_H_
#include "vlq.h"
#endif
#ifndef _MALLOC_H_
#include <malloc.h>
#endif
#ifndef _STRING_H_
#include <string.h>
#endif


size_t cachef_init(cachef *p, size_t capacity, const char *filename, cachef_mode mode) {
	ASSERT(filename);

	p->_f = fopen(filename, (mode == CACHEF_MODE_READ) ? "rb" : "wb");
	if (p->_f) {

		p->_cache = malloc(capacity);
		if (p->_cache) {

			p->_filename = (char*) malloc(strlen(filename) + 2);
			if (p->_filename) {

				strcpy(p->_filename, filename);
				p->_capacity = capacity;
				p->_idx = 0;
				p->_mode = mode;

				return 1;
			}

			free(p->_cache);
		}

		fclose(p->_f);
	}

	return 0;
}


void cachef_cleanup(cachef *p) {
	ASSERT(p->_f);
	ASSERT(p->_cache);
	ASSERT(p->_filename);

	cachef_flush(p);

	fclose(p->_f);
	free(p->_cache);
	free(p->_filename);
}


void cachef_flush(cachef *p) {

	if (p->_mode == CACHEF_MODE_WRITE) {

		if (p->_idx > 0) {

			fwrite(p->_cache, 1, p->_idx, p->_f);
			p->_idx = 0;

		}

	}

}


void cachef_write(cachef *p, const void *data, size_t num_bytes) {
	ASSERT(p->_mode == CACHEF_MODE_WRITE);

	if ((num_bytes + p->_idx) > p->_capacity) {
		cachef_flush(p);
	}

	memcpy((void*) ((size_t) p->_cache + p->_idx), data, num_bytes);
	p->_idx += num_bytes;

}


void cachef_write_vlq(cachef *p, size_t number) {
	ASSERT(p->_mode == CACHEF_MODE_WRITE);
	vlq_buf vlqb;

	if ((p->_idx + sizeof(size_t)) > p->_capacity) {
		cachef_flush(p);
	}

	vlqb.data = p->_cache;
	vlqb.idx = p->_idx;

	vlq_set(&vlqb);
	vlq_encode(number);

	p->_idx = vlqb.idx;

}


void cachef_write_vlq_signed(cachef *p, ptrdiff_t number) {
	ASSERT(p->_mode == CACHEF_MODE_WRITE);
	vlq_buf vlqb;

	if ((p->_idx + sizeof(size_t)) > p->_capacity) {
		cachef_flush(p);
	}

	vlqb.data = p->_cache;
	vlqb.idx = p->_idx;

	vlq_set(&vlqb);
	vlq_encode_signed(number);

	p->_idx = vlqb.idx;

}

