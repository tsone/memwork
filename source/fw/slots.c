/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include "slots.h"
#ifndef FW_H_
#include "fw.h"
#endif
#ifndef REAP_H_
#include "reap.h"
#endif
#ifndef INTERNAL_H_
#include "internal.h"
#endif
#ifndef _MALLOC_H_
#include <malloc.h>
#endif
#ifdef FW_TRACE_ORIG
#ifndef _STRING_H_
#include <string.h>
#endif
#endif


#ifdef FW_TRACE_ORIG

static size_t s_num_slots = 0;

#else

static reap s_slot_reap;

#endif


static void **s_slots = 0;


size_t slots_init(size_t num_slots) {
	ASSERT(!s_slots);
	ASSERT(num_slots > 0);

	s_slots = (void**) malloc(sizeof(void*) * num_slots);
	ASSERT(s_slots);

#ifdef FW_TRACE_ORIG

	memset(s_slots, 0, sizeof(void*) * num_slots);
	s_num_slots = num_slots;

#else

	REAP_INIT(s_slot_reap, s_slots, s_slots + num_slots);

#endif

	return 1;
}


void slots_set(size_t slot_idx, void *ptr) {
	ASSERT(slot_idx >= 0);
	ASSERT(ptr);
	ASSERT(s_slots);

#ifdef FW_TRACE_ORIG
	ASSERT(slot_idx < s_num_slots);
	ASSERT(s_slots[slot_idx] == 0);

	s_slots[slot_idx] = ptr;

#else
	void *slot;

	REAP_ALLOC(s_slot_reap, sizeof(void**), slot);
	ASSERT(slot);
	*((void**) slot) = ptr;

#endif

}


void* slots_get(size_t slot_idx) {
	void *result;
	ASSERT(s_slots);

#ifdef FW_TRACE_ORIG
	ASSERT(slot_idx < s_num_slots);
	ASSERT(s_slots[slot_idx]);

	result = s_slots[slot_idx];
	s_slots[slot_idx] = 0;

#else
	ASSERT(((void*) &s_slots[slot_idx]) < REAP_GET_END(s_slot_reap));

	result = s_slots[slot_idx];
	REAP_FREE(s_slot_reap, s_slots[slot_idx], sizeof(void**));

#endif

	ASSERT(result);
	return result;
}


void slots_cleanup() {
	ASSERT(s_slots);

	free(s_slots);
	s_slots = 0;
}
