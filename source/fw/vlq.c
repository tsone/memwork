/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include "vlq.h"


#define VLQ_ENCODE(n_, sh_, mask_) ( \
		(((n_) >= (((mask_) + 1) << (sh_))) ? 128 : 0) \
			| (((n_) >> (sh_)) & (mask_)) \
)


static vlq_buf *s_buf = 0;


void vlq_set(vlq_buf *buf) {
	s_buf = buf;
}


void vlq_encode(u32 number) {
	ASSERT(s_buf);
	ASSERT(number < (1 << 28));
	const size_t idx = s_buf->idx;

	if (number >= (1 << 14)) {
		if (number >= (1 << 21)) {
			s_buf->idx += 4;
			goto b4;
		} else {
			s_buf->idx += 3;
			goto b3;
		}
	} else {
		if (number >= (1 << 7)) {
			s_buf->idx += 2;
			goto b2;
		} else {
			s_buf->idx += 1;
			goto b1;
		}
	}

b4:
	s_buf->data[idx + 3] = VLQ_ENCODE(number, 21, 0x7F);
b3:
	s_buf->data[idx + 2] = VLQ_ENCODE(number, 14, 0x7F);
b2:
	s_buf->data[idx + 1] = VLQ_ENCODE(number, 7, 0x7F);
b1:
	s_buf->data[idx + 0] = VLQ_ENCODE(number, 0, 0x7F);

}


u32 vlq_decode() {
	ASSERT(s_buf);
	u32 result = 0;
	size_t shift = 0;
	size_t byte = 0;

	do {
		ASSERT(shift <= 21);
		byte = s_buf->data[s_buf->idx];

		result |= (byte & 0x7F) << shift;

		shift += 7;
		++s_buf->idx;

	} while (byte & 0x80);

	return result;
}


void vlq_encode_signed(s32 number) {
	ASSERT(s_buf);
	const u32 sign_bit = (number < 0) ? 0x40 : 0;
	union {
		s32 s;
		u32 u;
	} n = {
		number
	};

	if (sign_bit) {
		n.u = ~n.u;
	}
	n.u = (n.u & 0x3F) | ((n.u & (~0x3F)) << 1) | sign_bit;
	vlq_encode(n.u);

}


s32 vlq_decode_signed() {
	ASSERT(s_buf);
	union {
		u32 u;
		s32 s;
	} n = {
		vlq_decode()
	};
	const u32 sign_bit = (n.u & 0x40);

	n.u = (n.u & 0x3F) | ((n.u & (~0x7F)) >> 1);
	if (sign_bit) {
		n.u = ~n.u;
	}

	return n.s;
}

