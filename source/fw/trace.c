/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include "trace.h"
#ifndef FW_H_
#include "fw.h"
#endif
#ifndef INTERNAL_H_
#include "internal.h"
#endif


#define SHOW_PROGRESS	        0
#define CACHE_SIZE_BYTES        (8*1024)
#define OPS_OFFS                6

/* NOTE: Cache the whole file in other than ARM9. */
#ifdef ARM9
static u16 s_cache[CACHE_SIZE_BYTES / sizeof(u16)];
static size_t s_percent = 0;
#else
static u16 *s_cache = 0;
#endif
static size_t s_cache_cur = 0;
static size_t s_cache_end = 0;

static FILE *s_fi_trace = 0;
static long int s_flimit = 0;
static long int s_fpos = 0;


static size_t trace_cache_fill() {

#ifdef ARM9

	if (s_cache_cur >= s_cache_end) {

		ASSERT(s_fpos == ftell(s_fi_trace));

		size_t new_percent = (size_t) ((100 * s_fpos) / s_flimit);
		if (new_percent != s_percent) {
			s_percent = new_percent;

#if (SHOW_PROGRESS != 0)
			PRINT("\r%3u%%", s_percent);
#endif

		}

		if (s_fpos >= s_flimit) {
			// Trace file at end.

#if (SHOW_PROGRESS != 0)
			PRINT("\n");
#endif

			return 0;
		}

		// Read new page to cache.
		const size_t read_bytes = fread(s_cache, 1, sizeof(s_cache), s_fi_trace);
		ASSERT(!(read_bytes % (sizeof(u16))));
		s_cache_end = read_bytes / sizeof(u16);
		s_cache_cur = 0;
	}

#else

        if (s_fpos >= s_flimit) {
                // Trace at end.
                
                return 0;
        }

#endif

	return 1;
}


static void trace_cache_next(trace_op *op) {
        ASSERT(op);
	ASSERT(s_cache_cur < s_cache_end);

#ifdef FW_TRACE_ORIG

	op->slot = (size_t) s_cache[s_cache_cur];
	op->size = (size_t) s_cache[s_cache_cur + 1];
	s_cache_cur += 2;
	s_fpos += 2 * sizeof(u16);

#else

	op->size = (size_t) s_cache[s_cache_cur];
	++s_cache_cur;
	s_fpos += sizeof(u16);

	if (op->size & 0x8000) {
	        op->slot = op->size & 0x7FFF;
		op->size = TRACE_FREE_MAGIC;
	} else {
		// FIXME: Code breakdown here: assignment to slot is -1.
	        op->slot = -1;
		op->size = result;
	}

#endif

}


size_t trace_next(trace_op *op) {
        ASSERT(op);

	if (!trace_cache_fill()) {
	        /* No more data. */

		return 0;
	}

	trace_cache_next(op);

	return 1;
}


static size_t trace_get_num_ops() {

#ifdef FW_TRACE_ORIG

	return (s_flimit - OPS_OFFS) / 4;

#else

#error Not implemented.

#endif

}


size_t trace_init(fw_generator *generator, const char *in_file) {
	ASSERT(!s_fi_trace);
	u16 num_slots = 0;

	s_fi_trace = fopen(in_file, "rb");
	ASSERT(s_fi_trace);

#ifdef FW_TRACE_ORIG

	fseek(s_fi_trace, 0, SEEK_END);
	s_flimit = ftell(s_fi_trace);

	fseek(s_fi_trace, 4, SEEK_SET);
	fread(&num_slots, 1, sizeof(num_slots), s_fi_trace);

#else

	// Obtain num_slots from file footer and reset file position counter.
	fseek(s_fi_trace, -(sizeof(num_slots)), SEEK_END);
	fread(&num_slots, 1, sizeof(num_slots), s_fi_trace);
	s_flimit = ftell(s_fi_trace);
	rewind(s_fi_trace);

#endif

	s_fpos = ftell(s_fi_trace);

#ifndef ARM9

        ASSERT(s_fi_trace);
        ASSERT(s_fpos == OPS_OFFS);

        s_cache_end = s_flimit - s_fpos;
        s_cache_cur = 0;

        s_cache = malloc(s_cache_end);
        ASSERT(s_cache);

        fread(s_cache, 1, s_cache_end, s_fi_trace);
        ASSERT(ftell(s_fi_trace) == (s_cache_end + s_fpos));
        ASSERT(ftell(s_fi_trace) == s_flimit);

        fclose(s_fi_trace);
        s_fi_trace = 0;

#endif

        generator->num_slots = num_slots;
        generator->num_ops = trace_get_num_ops();

	return (size_t) num_slots;
}


void trace_cleanup() {

#ifdef ARM9

	ASSERT(s_fi_trace);
	fclose(s_fi_trace);
	s_fi_trace = 0;

#else

	ASSERT(!s_fi_trace);
        ASSERT(s_cache);
        free(s_cache);
        s_cache = 0;

#endif

}


void trace_rewind() {

#ifdef FW_TRACE_ORIG

#ifdef ARM9

	ASSERT(s_fi_trace);
	fseek(s_fi_trace, OPS_OFFS, SEEK_SET);
	s_fpos = ftell(s_fi_trace);

#else

        ASSERT(!s_fi_trace);
        ASSERT(s_cache);
        s_fpos = OPS_OFFS;
        s_cache_cur = 0;

#endif

#else

#error Not implemented.

#endif

}

