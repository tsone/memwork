/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include "reaps.h"
#ifndef STAT_H_
#include "stat.h"
#endif
#ifndef CDL_H_
#include "cdl.h"
#endif
#ifndef BIBOP_H_
#define BIBOP_PAGE_SH 10 /* =1 KB pages */
#include "bibop.h"
#endif

/* CONFIG: Define if prefer reusing pages that were last accessed. */
#define REAPS_CONFIG_PREFER_REUSE       1

#define REAPS_NUM_CLASSES		NUM_CLASSES


#define REAPS_PAGE_HEADER_SIZE	(sizeof(reaps_page_header))


#define REAPS_PAGE_HEADER_OFFSET \
		BIBOP_PAGE_HEADER_OFFSET(reaps_page_header)


#define REAPS_PAGE_HEADER_GET_FROM_PAGE(page_) \
		BIBOP_PAGE_HEADER((page_), reaps_page_header)


#ifndef NDEBUG
#define REAPS_UNUSED_FROM_CLASS_PTR(class_ptr_) \
		(s_unused_lookup[((class_ptr_) - s_p.classes) / sizeof(reaps_size_class)])
#endif


typedef struct t_reaps_size_class {

	cdl_node		page_ring;

} reaps_size_class;


typedef struct t_reaps_page_header {

	cdl_node page_ring;
	reap block_reap;
	reaps_size_class *size_class;
	size_t count;

} reaps_page_header;


typedef struct t_reaps {

	reaps_size_class classes[REAPS_NUM_CLASSES];
	bibop bop;

} reaps;


#ifndef NDEBUG
static size_t s_unused_lookup[REAPS_NUM_CLASSES];
#endif


static reaps s_p;


size_t reaps_init(void *start, size_t size_bytes) {
	size_t i;

	if (OFFSETOF(reaps_page_header, page_ring) || OFFSETOF(reaps_size_class, page_ring)) {

		/**
		 * Require reaps_page_header.page_ring and reaps_class.page_ring to have
		 * 0 offset. This is for optimization reasons.
		 */
		PERROR("Compiler problem prevents use of the allocator. See the source.\n");

		return 0;
	}

	BIBOP_INIT(s_p.bop, start, size_bytes);

	STAT_INIT(s_p.bop._start, sizeof(s_p));

	for (i = 0; i < REAPS_NUM_CLASSES; i++) {
		/* Initialize classes. */

		CDL_INIT(s_p.classes[i].page_ring);

#ifndef NDEBUG
		s_unused_lookup[i] = REAPS_PAGE_HEADER_OFFSET % CLASS_IDX_TO_SIZE(i);
#if 0
		PDEBUG("s_unused_lookup[%d] = %d\n", i, s_unused_lookup[i]);
#endif
#endif

	}

#if 0
	PDEBUG("REAPS_PAGE_HEADER_SIZE = %d\n", REAPS_PAGE_HEADER_SIZE);
#endif

	return BIBOP_GET_CAPACITY(s_p.bop);
}


void* reaps_malloc(size_t size_bytes) {
	ASSERT((size_bytes >= MIN_MALLOC) && (size_bytes <= MAX_MALLOC));
	size_t class_bytes = SIZE_TO_CLASS_IDX(size_bytes);
	ASSERT(class_bytes < REAPS_NUM_CLASSES);
	reaps_size_class *size_class = &s_p.classes[class_bytes];
	ASSERT((size_class >= s_p.classes)
			&& (size_class <= &s_p.classes[REAPS_NUM_CLASSES - 1]));
	class_bytes = CLASS_IDX_TO_SIZE(class_bytes);
	reaps_page_header *page_header;
	void *result;

	/* Do we have available reap page for this size class? */
	if (!CDL_IS_ORPHAN_UNSAFE(size_class->page_ring)) {
		/* Yes. There is a reap page for the size class. */

		page_header = (reaps_page_header*) CDL_NEXT(size_class->page_ring);

		BIBOP_ASSERT_PAGE_NOT_FREED(s_p.bop, BIBOP_GET_PAGE_FROM_PTR(s_p.bop, page_header));
		ASSERT(page_header->count <= (REAPS_PAGE_HEADER_OFFSET / SIZE(QUANTUM_SH)));

		++page_header->count;

		REAP_ALLOC_UNSAFE(page_header->block_reap, class_bytes, result);

		if (!REAP_CAN_ALLOCATE(page_header->block_reap, class_bytes)) {

			/* Safe version because reaps_free() needs node to be orphan. */
			CDL_REMOVE(page_header->page_ring);

		}

	} else {
		/* No. Get new reap page for the size class and return a block. */

		BIBOP_ALLOC_PAGE(s_p.bop, result);

		/* Increase implementation by header size. */
		STAT_ADD_IMPLEMENTATION(REAPS_PAGE_HEADER_SIZE);
		/* Increase internal fragmentation by space in region we can't use. */
		STAT_ADD_INTERNAL(REAPS_UNUSED_FROM_CLASS_PTR(size_class));

		/* Initialize page header. */
		page_header = REAPS_PAGE_HEADER_GET_FROM_PAGE(result);
		CDL_INSERT_AFTER(size_class->page_ring, page_header->page_ring);
		REAP_INIT(page_header->block_reap,
				(void*) ((size_t) result + class_bytes),
				(void*) page_header);
		page_header->size_class = size_class;
		page_header->count = 1;

	}

	ASSERT((void*) page_header > result);
	ASSERT((size_t) page_header + REAPS_PAGE_HEADER_SIZE <= (size_t) result + SIZE(BIBOP_PAGE_SH));

	STAT_HDR_INIT(result, size_bytes, class_bytes - size_bytes, 0);

	return result;
}


void reaps_free(void *ptr) {
	ASSERT(0 == (((size_t) ptr) & MASK(QUANTUM_SH)));
	BIBOP_ASSERT_VALID_PTR(s_p.bop, ptr);
	void *page = (void*) BIBOP_GET_PAGE_FROM_PTR(s_p.bop, ptr);
	BIBOP_ASSERT_PAGE_NOT_FREED(s_p.bop, page);
	reaps_page_header *page_header = REAPS_PAGE_HEADER_GET_FROM_PAGE(page);
	ASSERT((ptr >= page) && (ptr < (void*) page_header));
	ASSERT((page_header->size_class >= s_p.classes)
			&& (page_header->size_class <= &s_p.classes[REAPS_NUM_CLASSES - 1]));
	cdl_node *free_list;

	ASSERT((void*) page_header > page);
	ASSERT((size_t) page_header + REAPS_PAGE_HEADER_SIZE <= (size_t) page + SIZE(BIBOP_PAGE_SH));

	--page_header->count;
	ASSERT(page_header->count <= (REAPS_PAGE_HEADER_OFFSET / SIZE(QUANTUM_SH)));

	if (!page_header->count) {
		/* Count is zero, so we free the page. */

		/* Decrease implementation by header size. */
		STAT_ADD_IMPLEMENTATION(-REAPS_PAGE_HEADER_SIZE);

		/* Decrease internal fragmentation by space in region that can't be used. */
		STAT_ADD_INTERNAL(-REAPS_UNUSED_FROM_CLASS_PTR(page_header->size_class));

		ASSERT(!CDL_IS_ORPHAN(page_header->page_ring));
		CDL_REMOVE_UNSAFE(page_header->page_ring);

		BIBOP_FREE_PAGE(s_p.bop, page);

	} else {
		/* Page is still live and well after this free. */
		REAP_FREE(page_header->block_reap, ptr);

		if (CDL_IS_ORPHAN_UNSAFE(page_header->page_ring)) {
			/* This is first free when page became full in malloc(). */
			free_list = &(page_header->size_class->page_ring);

#ifdef REAPS_CONFIG_PREFER_REUSE

			CDL_INSERT_AFTER(*free_list, page_header->page_ring);

#else

                        CDL_INSERT_BEFORE(*free_list, page_header->page_ring);

#endif

		}

	}

}


void reaps_cleanup() {

	/* Do nothing. */

}
