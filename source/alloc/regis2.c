/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include "regis2.h"
#ifndef STAT_H_
#include "stat.h"
#endif


#define REGIS2_NUM_CLASSES			NUM_CLASSES

#define REGIS2_PAGE_HEADER_SIZE		sizeof(regis2_page_header)


#define REGIS2_PAGE_HEADER_OFFSET \
		BIBOP_PAGE_HEADER_OFFSET(regis2_page_header)


#define REGIS2_PAGE_HEADER_GET_FROM_PAGE(page_) \
		BIBOP_PAGE_HEADER((page_), regis2_page_header)


typedef struct t_regis2_class {

	region _bump;

} regis2_size_class;


typedef struct t_regis2_page_header {

	regis2_size_class *size_class;
	size_t count;

} regis2_page_header;


typedef struct t_regis2 {

	bibop bop;
	regis2_size_class size_classes[REGIS2_NUM_CLASSES];

} regis2;


static regis2 s_p;


size_t regis2_init(void *start, size_t size_bytes) {
	size_t i;

	BIBOP_INIT(s_p.bop, start, size_bytes);

	for (i = 0; i < REGIS2_NUM_CLASSES; i++) {
		/* Initialize classes. */

		REGION_INIT(s_p.size_classes[i]._bump, 0, 0);

	}

	STAT_INIT(s_p.bop._start, sizeof(s_p));

	return BIBOP_GET_CAPACITY(s_p.bop);

}


void* regis2_malloc(size_t size_bytes) {
	ASSERT((size_bytes >= MIN_MALLOC) && (size_bytes <= MAX_MALLOC));
	size_t new_size_bytes = SIZE_TO_CLASS_IDX(size_bytes);
	regis2_size_class *size_class = &s_p.size_classes[new_size_bytes];
	ASSERT((size_class >= s_p.size_classes)
			&& (size_class <= &s_p.size_classes[REGIS2_NUM_CLASSES - 1]));
	new_size_bytes = CLASS_IDX_TO_SIZE(new_size_bytes);
	regis2_page_header *page_header;
	void *result;

	REGION_ALLOC_UNSAFE(size_class->_bump, new_size_bytes, result);

	if (!REGION_ALLOC_FAILED(size_class->_bump)) {
		/* Allocate successful from size class region. */

		page_header = (regis2_page_header*) REGION_GET_END(size_class->_bump);
		++page_header->count;

	} else {
		/* Get new bumping page and initialize it. */

		BIBOP_ALLOC_PAGE(s_p.bop, result);

		/* Increase internal by remaining bump page bytes. */
		STAT_ADD_INTERNAL((ptrdiff_t) REGION_GET_END(size_class->_bump)
				- (((ptrdiff_t) REGION_GET_CUR(size_class->_bump)) - new_size_bytes));

		/* Increase implementation by header size. */
		STAT_ADD_IMPLEMENTATION(REGIS2_PAGE_HEADER_SIZE);

		/* Initialize page header. */
		page_header = REGIS2_PAGE_HEADER_GET_FROM_PAGE(result);
		page_header->size_class = size_class;
		page_header->count = 1;

		REGION_INIT(size_class->_bump,
				(void*) ((size_t) result + new_size_bytes),
				(void*) page_header);

	}

	STAT_HDR_INIT(result, size_bytes, new_size_bytes - size_bytes, 0);

	return result;
}


void regis2_free(void *ptr) {
	ASSERT(0 == (((size_t) ptr) & MASK(QUANTUM_SH)));
	BIBOP_ASSERT_VALID_PTR(s_p.bop, ptr);
	void *page = (void*) BIBOP_GET_PAGE_FROM_PTR(s_p.bop, ptr);
	BIBOP_ASSERT_PAGE_NOT_FREED(s_p.bop, page);
	regis2_page_header *page_header = REGIS2_PAGE_HEADER_GET_FROM_PAGE(page);
	regis2_size_class *size_class = page_header->size_class;
	ASSERT((size_class >= s_p.size_classes)
			&& (size_class <= &s_p.size_classes[REGIS2_NUM_CLASSES - 1]));
	ASSERT((ptr >= page) && (ptr < (void*) page_header));

	/**
	 * Add total size of block internal fragmentation. This is because freed
	 * Memory cannot be reused in region allocator.
	 */
	STAT_ADD_INTERNAL(INFO_HDR_GET_ALLOCATED(ptr) + INFO_HDR_GET_INTERNAL(ptr));

	--page_header->count;
	ASSERT(page_header->count <= (REGIS2_PAGE_HEADER_OFFSET / SIZE(QUANTUM_SH)));

	if (!page_header->count) {
		/* Live count is zero. Attempt to free page. */

 		BIBOP_FREE_PAGE(s_p.bop, page);

		/* Decrease implementation by header size. */
		STAT_ADD_IMPLEMENTATION(-sizeof(regis2_page_header));

		if (REGION_GET_END(size_class->_bump) != page_header) {
			/* We're not freeing the bump page. */

			/* Decrease internal by page capacity. */
			STAT_ADD_INTERNAL(-REGIS2_PAGE_HEADER_OFFSET);

		} else {
			/* Freeing bump page. Set bump page to invalid. */

			STAT_ADD_INTERNAL((ptrdiff_t) page - (ptrdiff_t) REGION_GET_CUR(size_class->_bump));
			REGION_SET_END(size_class->_bump, REGION_GET_CUR(size_class->_bump));

		}

	}

}


void regis2_cleanup() {

	/* Do nothing. */

}
