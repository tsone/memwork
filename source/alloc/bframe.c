/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
/**
 * Bitframe allocator implementation.
 *
 * INTRODUCTION
 * ------------
 *
 * Bitframe allocator is a bitmap-based allocator. Its malloc() and free()
 * are guaranteed to perform in O(1) time, making it suitable for real-time
 * applications.
 *
 * Bitframe allocator uses fixed-size 8-bit bitmaps or "bitframes" for
 * bookkeeping (hence the name). This eliminates bitmap search operations
 * which cause performance problems in regular bitmap-based allocators.
 *
 * Table lookup is used to search a suitable free block inside a bitframe.
 * This is a O(1) operation, so it's really fast. The down-side is that
 * allocations cannot span bitframe boundaries, i.e. cannot be larger than
 * 8 bits.
 *
 * However in practice, we need to reduce the maximum allocation further
 * down to 4 bits because of fragmentation issues. This means that for each
 * 8-bit bitframe we can allocate blocks spanning 4 bits at maximum.
 * However, 4 bits are enough to gain the main benefit from using bitmaps:
 * allocating blocks of variable size without storing headers!
 *
 * QUANTUM CALCULATION
 * -------------------
 *
 * For base quantum size of 8 bytes we need three levels (or size-classes)
 * to reach a maximum allocation of 512 bytes. Each level stores multiples
 * of some level quantum. Level 0 uses base quantum as its level quantum,
 * but next levels have quantum of 4x the previous level:
 *
 *     <level_quantum> = <base_quantum> * (4 ^ <level_index>)
 *     <level_range> = [<level_quantum>, <level_quantum> * 4]
 *     <level_sizes> = i * <level_quantum>, where i = 1,2,3,4
 *
 * Table 1: Three first levels for quantum of 8 w/ corresponding
 * size class quantum, size range and block sizes.
 *
 *     Lvl | Quantum | Range     | Sizes
 *     ----+---------+-----------+-------------------
 *     0   | 8       | 8 - 32    | 8, 16, 24, 32
 *     1   | 32      | 32 - 128  | 32, 64, 96, 128
 *     2   | 128     | 128 - 512 | 128, 256, 384, 512
 *     ----+---------+-----------+-------------------
 *
 * PAGE CALCULATION
 * ----------------
 *
 * Bitframes are stored in pages that contain a page header with necessary
 * bookkeeping data structures. The number of bitframes in page is limited
 * to 255. This is because bitframes are maintained in a doubly linked-list
 * w/ index to prev.
 *
 */
#include "bframe.h"
#ifndef STAT_H_
#include "stat.h"
#endif
#ifndef CDL_H_
#include "cdl.h"
#endif
#ifndef BIBOP_H_
#define BIBOP_PAGE_SH 14 /* =16 KB pages (large!) */
#include "bibop.h"
#endif


/**
 * Shift of maximum real length for blocks in bitframe.
 */
//#define BFRAME_RMAX_SH                2
#define BFRAME_RMAX_SH                  3


/**
 * Maximum real length for blocks in bitframe.
 */
#define BFRAME_RMAX_LENGTH              SIZE(BFRAME_RMAX_SH)


/**
 * Our quantum shift.
 */
#define BFRAME_QUANTUM_SH               QUANTUM_SH


/**
 * Number of levels in bitframe, i.e. the number of size classes.
 * For example, a base quantum of 8 bytes would yield first level capable
 * of storing 8-32 byte blocks. The next level would have level quantum of
 * 32 bytes and would store 33-128 blocks, etc.
 *
 */
#if (BFRAME_RMAX_LENGTH == 4)
#define BFRAME_NUM_LEVELS               3
#elif (BFRAME_RMAX_LENGTH == 8)
#define BFRAME_NUM_LEVELS               2
#endif


/**
 * Maximum total number of pages in a page.
 */
#define BFRAME_TOTAL_FRAMES             SIZE(BIBOP_PAGE_SH - 6)


/**
 * Number of pages in a page header. This is byte max minus page link heads
 * in page header.
 */
//#define BFRAME_NUM_PAGES              (((SIZE(BIBOP_PAGE_SH) - sizeof(bframe_page_header)) >> (3 + BFRAME_QUANTUM_SH)) - BFRAME_MAX_SIZE - 1 )
#define BFRAME_NUM_FRAMES               (BFRAME_TOTAL_FRAMES - BFRAME_RMAX_LENGTH)


/**
 * Total number of size classes.
 */
#define BFRAME_NUM_SIZE_CLASSES         (BFRAME_RMAX_LENGTH * BFRAME_NUM_LEVELS)


/**
 * Shortcut for bibop.
 */
#define BFRAME_GET_PAGE_HEADER(page_) \
		BIBOP_PAGE_HEADER(page_, bframe_page_header)


/**
 * Fixed-size bitmap aka "bitframe".
 * Stores allocation bits and sizes, and additionally links to previous and
 * next bitframes in linked list.
 */
typedef struct __attribute__ ((packed)) t_bframe_frame {

	u8 bits;
	u8 length_bits;
	u8 next;
	u8 prev;

} bframe_frame;


/**
 * Page header. Stores bitframes and bitframe statuses for each frame etc.
 */
typedef struct t_bframe_page_header {

        cdl_node	size_class_pages;

        /**
         * Bitframes in page.
         * Last BFRAME_RMAX_LENGTH items are heads to the doubly-linked list of
         * blocks on matching size capacity. For example, index
         * BFRAME_NUM_PAGES + 1 is the head for length 1.
         */
        bframe_frame    frames[BFRAME_TOTAL_FRAMES];

        size_t          quantum_shift;

        size_t	        head_bits;

        size_t	        size_class_idx;

} bframe_page_header;


/**
 * A level in bitframe allocator. Stores list of pages and quantum size of
 * the level.
 */
typedef struct t_bframe_size_class {

        cdl_node       size_class_pages;

} bframe_size_class;


/**
 * Bitframe data structure.
 */
typedef struct t_bframe {

        /**
         * Size class bits. Each level takes BFRAME_RMAX_LENGTH bits.
         * This uses total of BFRAME_NUM_SIZE_CLASSES bits.
         */
        size_t                  size_class_bits;

        /**
         * Size classes array.
         */
        bframe_size_class       size_classes[BFRAME_NUM_SIZE_CLASSES];

        bibop                   bop;

} bframe;


#if (BFRAME_RMAX_LENGTH == 8)

/**
 * Longest lookup for maximum length of 8.
 * Could use 3 bits for each item (except for index 255).
 */
static const u8 lookup_longest[] = {
7,6,5,5,4,4,4,4,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,
2,2,2,4,3,2,2,1,1,1,1,2,1,1,1,1,1,1,1,3,2,1,1,1,1,1,1,2,1,1,1,1,1,1,1,5,4,3,3,2,
2,2,2,2,1,1,1,1,1,1,1,3,2,1,1,1,0,0,0,2,1,0,0,1,0,0,0,4,3,2,2,1,1,1,1,2,1,0,0,1,
0,0,0,3,2,1,1,1,0,0,0,2,1,0,0,1,0,0,0,6,5,4,4,3,3,3,3,2,2,2,2,2,2,2,2,3,2,1,1,1,
1,1,1,2,1,1,1,1,1,1,1,4,3,2,2,1,1,1,1,2,1,0,0,1,0,0,0,3,2,1,1,1,0,0,0,2,1,0,0,1,
0,0,0,5,4,3,3,2,2,2,2,2,1,1,1,1,1,1,1,3,2,1,1,1,0,0,0,2,1,0,0,1,0,0,0,4,3,2,2,1,
1,1,1,2,1,0,0,1,0,0,0,3,2,1,1,1,0,0,0,2,1,0,0,1,0,0,8
};

#elif (BFRAME_RMAX_LENGTH == 4)

/**
 * Modified version for maximum length of 4.
 * Could use 2 bits for each item (except for index 255).
 */
static const u8 lookup_longest[] = {
3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,2,2,2,2,
2,2,2,3,3,2,2,1,1,1,1,2,1,1,1,1,1,1,1,3,2,1,1,1,1,1,1,2,1,1,1,1,1,1,1,3,3,3,3,2,
2,2,2,2,1,1,1,1,1,1,1,3,2,1,1,1,0,0,0,2,1,0,0,1,0,0,0,3,3,2,2,1,1,1,1,2,1,0,0,1,
0,0,0,3,2,1,1,1,0,0,0,2,1,0,0,1,0,0,0,3,3,3,3,3,3,3,3,2,2,2,2,2,2,2,2,3,2,1,1,1,
1,1,1,2,1,1,1,1,1,1,1,3,3,2,2,1,1,1,1,2,1,0,0,1,0,0,0,3,2,1,1,1,0,0,0,2,1,0,0,1,
0,0,0,3,3,3,3,2,2,2,2,2,1,1,1,1,1,1,1,3,2,1,1,1,0,0,0,2,1,0,0,1,0,0,0,3,3,2,2,1,
1,1,1,2,1,0,0,1,0,0,0,3,2,1,1,1,0,0,0,2,1,0,0,1,0,0,4
};

#else

#error "Unsupported BFRAME_RMAX_LENGTH."

#endif


/**
 * Lookup for the bit position of longest sequence.
 * Shared by all BFRAME_RMAX_LENGTH.
 */
static const u8 lookup_longest_idx[] = {
0,1,2,2,3,3,3,3,4,4,4,4,4,4,4,4,0,1,5,5,5,5,5,5,0,5,5,5,5,5,5,
5,0,1,2,2,0,3,3,3,0,1,6,6,0,6,6,6,0,1,2,2,0,6,6,6,0,1,6,6,0,6,6,6,0,1,2,2,3,3,3,
3,0,1,4,4,0,4,4,4,0,1,2,2,0,1,0,3,0,1,0,2,0,1,0,5,0,1,2,2,0,3,3,3,0,1,0,2,0,1,0,
4,0,1,2,2,0,1,0,3,0,1,0,2,0,1,0,7,0,1,2,2,3,3,3,3,0,4,4,4,4,4,4,4,0,1,2,2,0,5,5,
5,0,1,5,5,0,5,5,5,0,1,2,2,0,3,3,3,0,1,0,2,0,1,0,4,0,1,2,2,0,1,0,3,0,1,0,2,0,1,0,
6,0,1,2,2,3,3,3,3,0,1,4,4,0,4,4,4,0,1,2,2,0,1,0,3,0,1,0,2,0,1,0,5,0,1,2,2,0,3,3,
3,0,1,0,2,0,1,0,4,0,1,2,2,0,1,0,3,0,1,0,2,0,1,0,0
};


static bframe s_p;


#define LINK_IS_ORPHAN(p_, idx_) \
		((p_)[idx_].next == (idx_))

#define LINK_DETACH(p_, idx_) \
		(p_)[(p_)[idx_].prev].next = (p_)[idx_].next; \
		(p_)[(p_)[idx_].next].prev = (p_)[idx_].prev;

#define BFRAME_DETACH(page_header_, idx_, length_) do { \
		LINK_DETACH((page_header_).frames, (idx_)); \
		if (LINK_IS_ORPHAN((page_header_).frames, BFRAME_NUM_FRAMES + (length_))) { \
	        (page_header_).head_bits &= ~(1 << (length_)); \
		} \
} while (0)


#define LINK_ATTACH(p_, head_, idx_) do { \
		(p_)[idx_].prev = (head_); \
		(p_)[idx_].next = (p_)[head_].next; \
		(p_)[(p_)[head_].next].prev = (idx_); \
		(p_)[head_].next = (idx_); \
} while (0)

#define BFRAME_ATTACH(page_header_, idx_, length_) do { \
		LINK_ATTACH((page_header_).frames, BFRAME_NUM_FRAMES + (length_), (idx_)); \
		if (!LINK_IS_ORPHAN((page_header_).frames, BFRAME_NUM_FRAMES + (length_))) { \
	        (page_header_).head_bits |= (1 << (length_)); \
		} \
} while (0)


static __inline__ size_t size_class_idx_from_size(size_t size) {
	ASSERT(size >= 1);
	size_t result;

#if (BFRAME_RMAX_LENGTH == 4)

	/* BFRAME_NUM_LEVELS == 3 */
        if (size < SIZE((1 * BFRAME_RMAX_SH) + BFRAME_QUANTUM_SH)) {

                result = ((size - 1) >> ((0 * BFRAME_RMAX_SH) + BFRAME_QUANTUM_SH));

        } else if (size < SIZE((2 * BFRAME_RMAX_SH) + BFRAME_QUANTUM_SH)) {

                result = ((size - 1) >> ((1 * BFRAME_RMAX_SH) + BFRAME_QUANTUM_SH))
                                + (1 << (1 * BFRAME_RMAX_SH));

        } else {

                result = ((size - 1) >> ((2 * BFRAME_RMAX_SH) + BFRAME_QUANTUM_SH))
                                + (1 << (2 * BFRAME_RMAX_SH));

        }
/*
	size_t level_sh;
	size_t raw = (size - 1) >> BFRAME_QUANTUM_SH;

	__asm__ volatile (
			"bsr %1, %0"
			: "=r" (level_sh)
			: "r" (raw | 1)
			: "flags");

	// TODO: Fix magic (~1 and 2).
	size_t level_sh &= (~1);
	result = 2 * level_sh + (raw >> level_sh);

//	PDEBUG("size=%d level_sh=%d result=%d\n", size, level_sh, result);
*/

#elif (BFRAME_RMAX_LENGTH == 8)

	/* BFRAME_NUM_LEVELS == 2 */
	if (size < SIZE((1 * BFRAME_RMAX_SH) + BFRAME_QUANTUM_SH)) {

	        result = ((size - 1) >> ((0 * BFRAME_RMAX_SH) + BFRAME_QUANTUM_SH));

	} else {

                result = ((size - 1) >> ((1 * BFRAME_RMAX_SH) + BFRAME_QUANTUM_SH))
                                + (1 << (1 * BFRAME_RMAX_SH));

	}

#ifndef NDEBUG
	if (size > SIZE(BFRAME_NUM_LEVELS * BFRAME_RMAX_SH + BFRAME_QUANTUM_SH)) {
		result = BFRAME_NUM_SIZE_CLASSES;
	}
#endif

//	PDEBUG("size=%d result=%d\n", size, result);

#endif

	ASSERT(result < BFRAME_NUM_SIZE_CLASSES);

	return result;
}


static void bframe_page_init(bframe_page_header *p, size_t quantum_shift) {
	ASSERT(p);
	ASSERT((quantum_shift >= BFRAME_QUANTUM_SH)
			&& (quantum_shift <= BFRAME_QUANTUM_SH + BFRAME_RMAX_SH * BFRAME_NUM_LEVELS));
	size_t i;

	/**
	 * Calculate number of frames in this page. Number 3 comes from number
	 * of blocks in a frame, i.e. log2(8).
	 */
	size_t num_frames = (SIZE(BIBOP_PAGE_SH) - sizeof(bframe_page_header)) >> (3 + quantum_shift);
	if (num_frames > (BFRAME_TOTAL_FRAMES - BFRAME_RMAX_LENGTH)) {

//		PDEBUG("WARNING:Page cannot hold specified BFRAME_TOTAL_FRAMES number of frames.");

		num_frames = (BFRAME_TOTAL_FRAMES - BFRAME_RMAX_LENGTH);

	}
//	ASSERT(num_frames <= (BFRAME_TOTAL_FRAMES - BFRAME_RMAX_LENGTH));

	p->quantum_shift = quantum_shift;
//	PDEBUG("num_frames = %d, quantum_shift = %d\n", num_frames, p->quantum_shift);

	/* Init bitframes and their links. */
	/* FIXME: These loops need to be removed. */
	for (i = 0; i < num_frames; i++) {
		p->frames[i].bits = 0;
		p->frames[i].length_bits = 0;
		p->frames[i].prev = i - 1;
		p->frames[i].next = i + 1;
	}

	/* Init linked list heads except the last one. */
	for (i = BFRAME_TOTAL_FRAMES - BFRAME_RMAX_LENGTH; i < BFRAME_TOTAL_FRAMES - 1; i++) {
		p->frames[i].bits = 0xFF;			/* Just in case. */
		p->frames[i].length_bits = 0xFF;	/* Just in case. */
		p->frames[i].next = i;
		p->frames[i].prev = i;
	}

	/* Fix first and last frame links and link heads. */
	p->frames[0].prev = BFRAME_TOTAL_FRAMES - 1;
	p->frames[num_frames - 1].next = BFRAME_TOTAL_FRAMES - 1;
	p->frames[BFRAME_TOTAL_FRAMES - 1].next = 0;
	p->frames[BFRAME_TOTAL_FRAMES - 1].prev = num_frames - 1;

	/**
	 * Mark the initial full list with one-bit.
	 */
	p->head_bits = 1 << (BFRAME_RMAX_LENGTH - 1);

	STAT_ADD_IMPLEMENTATION(sizeof(bframe_page_header));
	STAT_ADD_INTERNAL(BIBOP_PAGE_HEADER_OFFSET(bframe_page_header)
			- ((8 * num_frames) << p->quantum_shift));

}


static bframe_page_header *new_page_for_level(size_t level_idx) {

//	PDEBUG("New page start: level_idx = %d size_class_bits = %08X\n",
//			level_idx, s_p.size_class_bits);

	bframe_page_header *page_header;

	/* Allocate and init page. */
	BIBOP_ALLOC_PAGE(s_p.bop, page_header);

	page_header = BFRAME_GET_PAGE_HEADER(page_header);
	bframe_page_init(page_header, BFRAME_QUANTUM_SH + BFRAME_RMAX_SH * level_idx);

	/* Set page ring links. */
	page_header->size_class_idx = (level_idx << BFRAME_RMAX_SH) + BFRAME_RMAX_LENGTH - 1;
	CDL_INSERT_AFTER(
			s_p.size_classes[page_header->size_class_idx].size_class_pages,
			page_header->size_class_pages);

	/* Set size class bits. */
	s_p.size_class_bits |= (1 << page_header->size_class_idx);

//	PDEBUG("New page end: size_class_bits = %08X\n",
//			s_p.size_class_bits);

	return page_header;
}


static __inline__ void bframe_page_fix_list(bframe_page_header *page_header, size_t idx,
                size_t longest) {
        ASSERT(page_header);
        ASSERT(idx < BFRAME_NUM_FRAMES);
        ASSERT(longest <= BFRAME_RMAX_LENGTH);

#if 1

        /* This is because lookup table does not have 0xFF. */
        size_t new_longest = (page_header->frames[idx].bits != 0xFF)
                ? lookup_longest[page_header->frames[idx].bits]
                : BFRAME_RMAX_LENGTH;

#else

        size_t new_longest = lookup_longest[page_header->frames[idx].bits];

#endif

        /* Remove frame from ring if frame used to belong to one. */
        if (longest < BFRAME_RMAX_LENGTH) {

                BFRAME_DETACH(*page_header, idx, longest);

        }

        /* Attach frame to a ring if frame must belong to one. */
        if (new_longest < BFRAME_RMAX_LENGTH) {

                BFRAME_ATTACH(*page_header, idx, new_longest);

        }

	size_t new_size_class_idx;

	/* Remove the page from ring. It's no-op if page is already orphan. */
	CDL_REMOVE(page_header->size_class_pages);
        if (CDL_IS_ORPHAN_UNSAFE(s_p.size_classes[page_header->size_class_idx].size_class_pages)) {

                s_p.size_class_bits &= ~(1 << page_header->size_class_idx);

        }

	if (page_header->head_bits) {
    	/* Page has free blocks left. Insert page to suitable ring. */

		/* Find page longest. */
		__asm__ volatile (
				"bsr %1, %0"
				: "=r" (new_size_class_idx)
				: "r" (page_header->head_bits)
				: "flags"
		);
		ASSERT(new_size_class_idx < BFRAME_RMAX_LENGTH);

		/* Calculate new size class and insert to that ring. */
		new_size_class_idx |=
				(page_header->size_class_idx & (~MASK(BFRAME_RMAX_SH)));
		ASSERT(new_size_class_idx < BFRAME_NUM_SIZE_CLASSES);
		CDL_INSERT_AFTER(s_p.size_classes[new_size_class_idx].size_class_pages,
				page_header->size_class_pages);
		page_header->size_class_idx = new_size_class_idx;

		/* Set size class bit accordingly. */
		s_p.size_class_bits |= (1 << new_size_class_idx);

	}

}


size_t bframe_init(void *start, size_t size_bytes) {
	size_t i;

	if (OFFSETOF(bframe_page_header, size_class_pages) || OFFSETOF(bframe_size_class, size_class_pages)) {

	        /**
		 * Require bframe_page_header.page_ring to have 0 offset.
		 * This is as optimization.
		 */
		PERROR("Compiler problem prevents use of the allocator. See the source.\n");

		return 0;
	}

	BIBOP_INIT(s_p.bop, start, size_bytes);

	STAT_INIT(s_p.bop._start, sizeof(s_p) + sizeof(lookup_longest_idx) + sizeof(lookup_longest));

	for (i = 0; i < BFRAME_NUM_SIZE_CLASSES; i++) {

		CDL_INIT(s_p.size_classes[i].size_class_pages);

	}

	s_p.size_class_bits = (1 << BFRAME_NUM_SIZE_CLASSES);
	for (i = 0; i < BFRAME_NUM_LEVELS; i++) {

		new_page_for_level(i);

	}

#if 0

	PDEBUG("sizeof(bframe_page_header) = %d\n", sizeof(bframe_page_header));
	PDEBUG("STAT_INFO._implementation = %d\n", STAT_INFO._implementation);
	PDEBUG("--\n");

	size_class_idx_from_size(1);
	size_class_idx_from_size(8);
	size_class_idx_from_size(9);
	size_class_idx_from_size(16);
	size_class_idx_from_size(17);
	size_class_idx_from_size(24);
	size_class_idx_from_size(25);
	size_class_idx_from_size(31);
	size_class_idx_from_size(32);
	size_class_idx_from_size(33);
	size_class_idx_from_size(63);
	size_class_idx_from_size(64);
	size_class_idx_from_size(65);
	size_class_idx_from_size(96);
	size_class_idx_from_size(97);
	size_class_idx_from_size(128);
	size_class_idx_from_size(129);
	size_class_idx_from_size(256);
	size_class_idx_from_size(257);
	size_class_idx_from_size(512);

	return 0;
#endif

	return BIBOP_GET_CAPACITY(s_p.bop);
}


static __inline__ size_t get_head_idx(size_t head_bits, size_t idx) {
	size_t result;

	ASSERT(head_bits >> idx);
//	ASSERT(head_bits & (~MASK(idx)));
	__asm__ volatile (
			"bsf %1, %0"
			: "=r" (result)
			: "r" (head_bits >> idx)
//			: "r" (head_bits & (~MASK(idx)))
			: "flags"
	);

//	return result;
	return result + idx;
}


static __inline__ void* bframe_page_malloc(bframe_page_header *page_header,
                size_t head_idx, size_t length) {

#ifndef NDEBUG
        /* TODO: Remove assertions for debug. */
        size_t i;
        for (i = 0; i < BFRAME_RMAX_LENGTH; i++) {
                if (page_header->head_bits & (1 << i)) {
                        ASSERT(!LINK_IS_ORPHAN(page_header->frames, BFRAME_NUM_FRAMES + i));
                        ASSERT(lookup_longest[page_header->frames[page_header->frames[BFRAME_NUM_FRAMES + i].next].bits] == i);
                } else {
                        ASSERT(LINK_IS_ORPHAN(page_header->frames, BFRAME_NUM_FRAMES + i));
                }
        }
#endif

        ASSERT(page_header->head_bits & MASK(BFRAME_RMAX_LENGTH));

        ASSERT((head_idx >= BFRAME_NUM_FRAMES) && (head_idx < BFRAME_TOTAL_FRAMES));
        ASSERT(!LINK_IS_ORPHAN(page_header->frames, head_idx));
        size_t idx = page_header->frames[head_idx].next;

        ASSERT(page_header->frames[idx].bits != 0xFF);
        size_t longest = lookup_longest[page_header->frames[idx].bits];
        ASSERT(longest < BFRAME_RMAX_LENGTH);

        /* Set bitframe allocation and length bits. */
        size_t bit_idx = lookup_longest_idx[page_header->frames[idx].bits];
        page_header->frames[idx].bits |= ((0x02 << length) - 1) << bit_idx;
        page_header->frames[idx].length_bits |= 1 << (length + bit_idx);

        /* if longest sequence changed, move bitframe to another heads list */
        bframe_page_fix_list(page_header, idx, longest);

        /* Compute block pointer. */
        return (void*) (((size_t) BIBOP_GET_PAGE_FROM_PTR(s_p.bop, page_header))
                        + ((bit_idx + (idx << 3)) << page_header->quantum_shift));
}


void* bframe_malloc(size_t size_bytes) {
        ASSERT(size_bytes >= MIN_MALLOC && size_bytes <= MAX_MALLOC);
        bframe_page_header *page_header;
        size_t size_class_idx = size_class_idx_from_size(size_bytes);
        ASSERT(size_class_idx < BFRAME_NUM_SIZE_CLASSES);

        /* Scan for next suitable size class. */
        size_t best_size_class_idx = (s_p.size_class_bits >> size_class_idx);
        ASSERT(best_size_class_idx);
        __asm__ volatile (
                "bsf %1, %0"
                : "=r" (best_size_class_idx)
                : "r" (best_size_class_idx)
                : "flags");
        best_size_class_idx += size_class_idx;

        if ((best_size_class_idx ^ size_class_idx) < BFRAME_RMAX_LENGTH) {
                /* Best size class idx is on requested level. */

                ASSERT(!CDL_IS_ORPHAN_UNSAFE(s_p.size_classes[best_size_class_idx].size_class_pages));
                page_header = (bframe_page_header*) CDL_NEXT(
                                s_p.size_classes[best_size_class_idx].size_class_pages);

        } else {
                /**
                 * Best size class idx is larger than requested level, or that pages
		 * have been used.
		 */
//		PDEBUG("Xor = %08X\n", (best_size_class_idx ^ size_class_idx));
                page_header = new_page_for_level(size_class_idx >> BFRAME_RMAX_SH);
                if (!page_header) {

                        return 0;
                }

        }

        /* Locate suitable bitframe from heads list. */
        size_t length = ((size_bytes - 1) >> page_header->quantum_shift);
        ASSERT(length < BFRAME_RMAX_LENGTH);

        /* Find list with a free block. Should always succeed. */
        size_t head_idx = get_head_idx(page_header->head_bits, length);
        void *result = bframe_page_malloc(page_header, BFRAME_NUM_FRAMES + head_idx, length);

        STAT_HDR_INIT(
                result,
                size_bytes,
                SIZE(page_header->quantum_shift) * (length + 1) - size_bytes,
                0
        );

        return (void*) result;
}


void bframe_free(void *ptr) {

	/* Compute page stuff from ptr. */
	void *page = (void*) BIBOP_GET_PAGE_FROM_PTR(s_p.bop, ptr);
	bframe_page_header *page_header = BFRAME_GET_PAGE_HEADER(page);

	/* Compute bitframe idx and bit_idx from ptr. */
	size_t idx = (((size_t) ptr - (size_t) page) >> page_header->quantum_shift);
	size_t bit_idx = idx & 7;
	idx = idx >> 3;

#if 0

	/* This is because lookup table does not have 0xFF. */
	size_t longest = (page_header->frames[idx].bits != 0xFF)
			? lookup_longest[page_header->frames[idx].bits]
			: BFRAME_RMAX_LENGTH;

#else

	size_t longest = lookup_longest[page_header->frames[idx].bits];

#endif

	/* Free blocks; first get size and then clear bits according to length. */
	size_t mask = (~0) << bit_idx;
	size_t max_bit;

	ASSERT(page_header->frames[idx].length_bits & mask);
	__asm__ volatile (
			"bsf %1, %0"
			: "=r" (max_bit)
			: "r" (page_header->frames[idx].length_bits & mask)
			: "flags"
	);
	ASSERT(max_bit < (bit_idx + BFRAME_RMAX_LENGTH));
	mask ^= ((2 << max_bit) - 1);

	page_header->frames[idx].bits &= mask;
	page_header->frames[idx].length_bits &= mask;

	/* If longest sequence changed, move bitframe to another heads list */
	bframe_page_fix_list(page_header, idx, longest);

#if 0
	PDEBUG("- %08X %08X idx=%d bit_idx=%d length=%d longest=%d headbits=%02X\n",
		(size_t) page_header, (size_t) ptr, idx, bit_idx, size, longest, page_header->head_bits);
#endif
}


void bframe_cleanup() {

	/* Do nothing. */

}
