/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include "sss.h"
#ifndef STAT_H_
#include "stat.h"
#endif
#ifndef REGION_H_
#include "region.h"
#endif
#ifndef LIST_H_
#include "list.h"
#endif


/* CONFIG: Undefine to force no debug info. */
//#define SSS_DEBUG


#ifdef SSS_DEBUG
#define SSS_HEADER_MAGIC	0xFEFEFEFE
#endif


#define SSS_HEADER_SIZE			ALIGN_UP_TO_CLASS(sizeof(sss_header))


#define SSS_NUM_SIZE_CLASSES	(SIZE_TO_CLASS_IDX(MAX_MALLOC + SSS_HEADER_SIZE) + 1)


typedef struct t_sss_size_class {

	list free_list;

} sss_size_class;


typedef struct t_sss_header {

	/**
	 * This is to pad size_class so that free list doesn't overwrite it.
	 * This is a slight optimization.
	 */
#ifdef SSS_DEBUG
	size_t magic;
#else
	void *dummy_pad;
#endif

	sss_size_class *size_class;

} sss_header;


typedef struct t_sss {

	sss_size_class size_classes[SSS_NUM_SIZE_CLASSES];
	region heap_region;

} sss;


static sss s_p;


size_t sss_init(void *start, size_t size_bytes) {
	size_t i;
	void* base = (void*) ALIGN_UP_TO_CLASS((size_t) start);

	if (OFFSETOF(sss_header, size_class) != sizeof(void*)) {

		/**
		 * Require size_class to be padded by a pointer size.
		 * This is an optimization.
		 *
		 */
		PERROR("Compiler problem prevents to use the allocator. See the source.\n");

		return 0;
	}

	REGION_INIT(s_p.heap_region, base, (void*) (ALIGN_DOWN((size_t) base + size_bytes, QUANTUM_SH)));

	STAT_INIT(REGION_GET_CUR(s_p.heap_region), sizeof(s_p));

	for (i = 0; i < SSS_NUM_SIZE_CLASSES; i++) {

		LIST_INIT(s_p.size_classes[i].free_list);

	}

#if 0
	PDEBUG("SSS_HEADER_SIZE = %d\n", SSS_HEADER_SIZE);
	PDEBUG("sizeof(s_p) = %d\n", sizeof(s_p));
	return 0;
#endif

	return REGION_GET_REMAINING(s_p.heap_region);
}


void* sss_malloc(size_t size_bytes) {
	ASSERT((size_bytes >= MIN_MALLOC) && (size_bytes <= MAX_MALLOC));
	size_t real_size = SIZE_TO_CLASS_IDX(size_bytes + SSS_HEADER_SIZE);
	sss_size_class *size_class = &s_p.size_classes[real_size];
	real_size = CLASS_IDX_TO_SIZE(real_size);
	sss_header *header;

	if (LIST_HAS_ITEMS(size_class->free_list)) {
		/* We have suitable block on free list. */

		/* Pop block from free list. */
		LIST_POP(size_class->free_list, header);

#ifdef SSS_DEBUG
		/* Magic must contain pointer here etc. thus != SSS_HEADER_MAGIC. */
		ASSERT(header->magic != SSS_HEADER_MAGIC);
#endif

	} else {
		/* No blocks in free list. Bump new block from region. */

		REGION_ALLOC_UNSAFE(s_p.heap_region, real_size, header);
		if (REGION_ALLOC_FAILED(s_p.heap_region)) {
			/* Heap exhausted. */

			return 0;
		}

		/* Set block size_class for sss_free(). */
		header->size_class = size_class;

	}

#ifdef SSS_DEBUG
	header->magic = SSS_HEADER_MAGIC;
#endif

	header = (sss_header*) ((size_t) header + SSS_HEADER_SIZE);
	STAT_HDR_INIT((void*) header, size_bytes,
				real_size - size_bytes - SSS_HEADER_SIZE,
				SSS_HEADER_SIZE);
	return (void*) header;
}


void sss_free(void *ptr) {
	sss_header *header = (sss_header*) ((size_t) ptr - SSS_HEADER_SIZE);
	sss_size_class *size_class = header->size_class;
	ASSERT((size_class >= s_p.size_classes)
			&& (size_class <= &s_p.size_classes[SSS_NUM_SIZE_CLASSES - 1]));

#ifdef SSS_DEBUG
	ASSERT(header->magic == SSS_HEADER_MAGIC);
	header->magic = ~SSS_HEADER_MAGIC;
#endif

	LIST_PUSH(size_class->free_list, header);
//	CDL_INSERT_AFTER(size_class->free_list, header->free_list);

}


void sss_cleanup() {

	/* Do nothing. */

}
