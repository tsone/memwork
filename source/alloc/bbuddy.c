/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include "bbuddy.h"
#ifndef CDL_H_
#include "cdl.h"
#endif
#ifndef STAT_H_
#include "stat.h"
#endif


/**
 * Number of size classes. +1 is needed for direct indexing and another
 * +1 because buddies need to store header.
 */
#define BBUDDY_NUM_CLASSES              (MAX_MALLOC_SH + 2)

#define BBUDDY_HEADER_SIZE		ALIGN_UP_TO_CLASS(sizeof(bbuddy_header))

/** MSB of buddy header live_flags marks block as live or not. */
#define BBUDDY_HEADER_LIVE_BIT          ((((size_t) -1) >> 1) + 1)


/**
 * 5 bits store k in live_flags header. Integers 0-31 can split the whole
 * 32-bit address range.
 */
#define BBUDDY_HEADER_K_MASK	0x1F


#define BBUDDY_HEADER_IS_LIVE(p_) \
		((p_).live_flags & BBUDDY_HEADER_LIVE_BIT)


#define BBUDDY_HEADER_GET_K(p_) \
		((p_).live_flags & BBUDDY_HEADER_K_MASK)


#define BBUDDY_MAKE_K_BIT(k_) \
		(1 << (k_))


#define BBUDDY_GET_BUDDY(header_ptr_, k_bit_) \
		((bbuddy_header*) (((size_t) (header_ptr_)) ^ (k_bit_)))


typedef struct t_bbuddy_header {

	cdl_node free_list;
	size_t live_flags;

} bbuddy_header;


typedef struct t_bbuddy {

	cdl_node class_free[BBUDDY_NUM_CLASSES];
	size_t class_free_bits;

	size_t header_size;
	size_t min_k;
	size_t max_k;


} bbuddy;


static bbuddy s_p;


/**
 * Get k from size (=n).
 * NOTE: Does not work for n <= 1!
 */
static __inline__ size_t calc_k_from_size(size_t n) {
	ASSERT(n > 1);
	size_t x;

	__asm__ volatile (
		"bsr %1, %0"
		: "=r" (x)
		: "r" ((n - 1) | 1)
		: "flags"
	);

	return x + 1;
}


size_t bbuddy_init(void *start, size_t size_bytes) {
	ASSERT(start && (size_bytes > 0));
	size_t i;
	size_t page_size;
	size_t real_start;
	size_t end;

	if (OFFSETOF(bbuddy_header, free_list)) {

		/* Require that header free_list list prev node and live_flags flags offset is 0.
		 * BBUDDY_HEADER_LIVE_BIT is set for live_flags buddies which override prev node. */
		PERROR("Compiler problem prevents to use the allocator. See the source.\n");

		return 0;
	}

	s_p.min_k = calc_k_from_size(BBUDDY_HEADER_SIZE + SIZE(QUANTUM_SH));
	s_p.max_k = calc_k_from_size(MAX_MALLOC + BBUDDY_HEADER_SIZE);
	ASSERT(s_p.max_k < BBUDDY_NUM_CLASSES);

	page_size = BBUDDY_MAKE_K_BIT(s_p.max_k);

	real_start  = ALIGN_UP((size_t) start, s_p.max_k);
	end = ALIGN_DOWN(real_start + size_bytes, s_p.max_k);

	/* Initialize class free_list lists. */
	for (i = 0; i < BBUDDY_NUM_CLASSES; i++) {

		CDL_INIT(s_p.class_free[i]);

	}

	/* Split heap to pages and insert to class free_list lists. */
	for (i = end - page_size; i >= real_start; i -= page_size) {

		bbuddy_header *b = (bbuddy_header*) i;
		CDL_INSERT_AFTER(s_p.class_free[s_p.max_k], b->free_list);
		b->live_flags = s_p.max_k;

	}

	/**
	 * Set bits at index max_k and max_k + 1. The latter bit is to check
	 * if no suitable buddies are available.
	 */
	s_p.class_free_bits = BBUDDY_MAKE_K_BIT(s_p.max_k)
	                | BBUDDY_MAKE_K_BIT(s_p.max_k + 1);

#if 0
	/* DEBUG: Check struct sizes. */
	PDEBUG("sizeof(bbuddy_header) = %d\n", sizeof(bbuddy_header));
	PDEBUG("--\n");

	PDEBUG("live_header_size = %d\n", BBUDDY_HEADER_SIZE);
	PDEBUG("min_k = %d\n", s_p.min_k);
	PDEBUG("max_k = %d\n", s_p.max_k);
	PDEBUG("page_size = %d\n", page_size);
	PDEBUG("--\n");

	PDEBUG("get_k(2) = %d\n", calc_k_from_size(2));
	PDEBUG("get_k(3) = %d\n", calc_k_from_size(3));
	PDEBUG("get_k(4) = %d\n", calc_k_from_size(4));
	PDEBUG("get_k(5) = %d\n", calc_k_from_size(5));
	PDEBUG("get_k(7) = %d\n", calc_k_from_size(7));
	PDEBUG("get_k(8) = %d\n", calc_k_from_size(8));
	PDEBUG("get_k(9) = %d\n", calc_k_from_size(9));
	PDEBUG("get_k(15) = %d\n", calc_k_from_size(15));
	PDEBUG("get_k(16) = %d\n", calc_k_from_size(16));
	PDEBUG("get_k(17) = %d\n", calc_k_from_size(17));
	PDEBUG("--\n");
	return 0;
#endif

	STAT_INIT((void*) real_start, sizeof(s_p));

	return end - real_start;
}


void* bbuddy_malloc(size_t size_bytes) {
	bbuddy_header *header;
	bbuddy_header *buddy;
	size_t k = calc_k_from_size(size_bytes + BBUDDY_HEADER_SIZE);
	ASSERT(k <= s_p.max_k);
	size_t k_mask;
	size_t i;
	size_t i_bit;

	if (k < s_p.min_k) {

		k = s_p.min_k;

	}

	k_mask = ~(BBUDDY_MAKE_K_BIT(k) - 1);

	/* Find list with a free buddy. */
	__asm__ volatile ("bsf %1, %0"
			: "=r" (i)
			: "r" (s_p.class_free_bits & k_mask)
			: "flags");
        ASSERT((i >= s_p.min_k) && (i >= k));

	if (i > s_p.max_k) {
	        /* No suitable buddy found. Heap exhausted. */

	        return 0;
	}

	i_bit = BBUDDY_MAKE_K_BIT(i);

	/* Remove our buddy from free list.
	 * This buddy is actually returned by this malloc,
	 * but we need to split it to remaining buddies.
	 */
	ASSERT(!CDL_IS_ORPHAN(s_p.class_free[i]));
	header = (bbuddy_header*) CDL_NEXT(s_p.class_free[i]);
	CDL_REMOVE_UNSAFE(header->free_list);
	if (CDL_IS_ORPHAN_UNSAFE(s_p.class_free[i])) {

		s_p.class_free_bits &= ~(i_bit);

	}

	/* Set free list bits in advance. No need to set in loop. */
	s_p.class_free_bits |= ((i_bit - 1) & k_mask);

	/* Split buddies. */
	while (i > k) {
		/* Iterate until we have correct size buddy. */

		--i;
		i_bit >>= 1;

		/* Add buddy's header to free list but continue with current header. */
		buddy = BBUDDY_GET_BUDDY(header, i_bit);
		CDL_INSERT_AFTER(s_p.class_free[i], buddy->free_list);
		buddy->live_flags = i;

	}

	/* Initialize header. */
	header->live_flags = BBUDDY_HEADER_LIVE_BIT | i;

	header = (bbuddy_header*) ((size_t) header + BBUDDY_HEADER_SIZE);

	STAT_HDR_INIT((void*) header, size_bytes,
			i_bit - size_bytes - BBUDDY_HEADER_SIZE,
			BBUDDY_HEADER_SIZE);

	return header;
}


void bbuddy_free(void *ptr) {
	bbuddy_header *header = (bbuddy_header *) ((size_t) ptr - BBUDDY_HEADER_SIZE);
//	ASSERT((size_t) header >= s_p.start && (size_t) ptr < s_p.end);
	ASSERT(BBUDDY_HEADER_IS_LIVE(*header));
	size_t k = BBUDDY_HEADER_GET_K(*header);
	ASSERT((k >= s_p.min_k) && (k <= s_p.max_k));
	size_t k_bit = BBUDDY_MAKE_K_BIT(k);
        bbuddy_header *buddy = (bbuddy_header*) ((size_t) header ^ k_bit);
        size_t inv_free_bits = ~s_p.class_free_bits;

        /* Merge buddies. */
        while (!BBUDDY_HEADER_IS_LIVE(*buddy)
			&& (BBUDDY_HEADER_GET_K(*buddy) == k)
			&& (k < s_p.max_k)) {

		CDL_REMOVE_UNSAFE(buddy->free_list);

		if (CDL_IS_ORPHAN_UNSAFE(s_p.class_free[k])) {
		        inv_free_bits |= k_bit;
		}

		++k;
		k_bit <<= 1;

		if (buddy < header) {

		        header = buddy;

		}

		buddy = (bbuddy_header*) ((size_t) header ^ k_bit);

	}

	CDL_INSERT_AFTER(s_p.class_free[k], header->free_list);
	header->live_flags = k;

	s_p.class_free_bits = (~inv_free_bits) | k_bit;

}


void bbuddy_cleanup() {

	/* Do nothing... */

}
