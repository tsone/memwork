/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#include "regis1.h"


#define REGIS1_PAGE_HEADER_OFFSET \
		BIBOP_PAGE_HEADER_OFFSET(regis1_page_header)


#define REGIS1_PAGE_HEADER_GET_FROM_PAGE(page_) \
		BIBOP_PAGE_HEADER((page_), regis1_page_header)


typedef struct t_regis1_page_header {

	size_t count;

} regis1_page_header;


typedef struct t_regis1 {

	bibop	bop;
	region	_bump;

} regis1;


static regis1 s_p;


size_t regis1_init(void *start, size_t size_bytes) {

	BIBOP_INIT(s_p.bop, start, size_bytes);

	REGION_INIT(s_p._bump, 0, 0);

	STAT_INIT(s_p.bop._start, sizeof(s_p));

	return BIBOP_GET_CAPACITY(s_p.bop);

}


void* regis1_malloc(size_t size_bytes) {
	ASSERT((size_bytes >= MIN_MALLOC) && (size_bytes <= MAX_MALLOC));
	size_t new_size_bytes = ALIGN_UP_TO_CLASS(size_bytes);
	regis1_page_header *page_header;
	void *result;

	REGION_ALLOC_UNSAFE(s_p._bump, new_size_bytes, result);

	if (!REGION_ALLOC_FAILED(s_p._bump)) {
		/* Region allocation succeeded. */

		page_header = (regis1_page_header*) REGION_GET_END(s_p._bump);
		++page_header->count;

	} else {
		/* Region is full. Allocate new page. */

		BIBOP_ALLOC_PAGE(s_p.bop, result);

		/* Increase internal by remaining bump page bytes. */
		STAT_ADD_INTERNAL((ptrdiff_t) REGION_GET_END(s_p._bump)
				- (((ptrdiff_t) REGION_GET_CUR(s_p._bump)) - new_size_bytes));

		STAT_ADD_IMPLEMENTATION(sizeof(regis1_page_header));

		/* Initialize page header. */
		page_header = REGIS1_PAGE_HEADER_GET_FROM_PAGE(result);
		page_header->count = 1;

		REGION_INIT(s_p._bump,
				(void*) ((size_t) result + new_size_bytes),
				(void*) page_header);

	}

	STAT_HDR_INIT(result, size_bytes, new_size_bytes - size_bytes, 0);

	return result;
}


void regis1_free(void *ptr) {
	ASSERT(0 == (((size_t) ptr) & MASK(QUANTUM_SH)));
	BIBOP_ASSERT_VALID_PTR(s_p.bop, ptr);
	void *page = (void*) BIBOP_GET_PAGE_FROM_PTR(s_p.bop, ptr);
	BIBOP_ASSERT_PAGE_NOT_FREED(s_p.bop, page);
	regis1_page_header *page_header = REGIS1_PAGE_HEADER_GET_FROM_PAGE(page);
	ASSERT((ptr >= page) && (ptr < (void*) page_header));

	/* Add total size of block internal fragmentation. This is because freed
	 * memory cannot be reused in region allocator.
	 */
	STAT_ADD_INTERNAL(INFO_HDR_GET_ALLOCATED(ptr) + INFO_HDR_GET_INTERNAL(ptr));

	--page_header->count;
	ASSERT(page_header->count <= (REGIS1_PAGE_HEADER_OFFSET / SIZE(QUANTUM_SH)));

	if (!page_header->count) {
		/* Zero live count. Free the page. */

		if (REGION_GET_END(s_p._bump) != page_header) {
			/* Page is not bump page. Free the page. */

			STAT_ADD_INTERNAL(-REGIS1_PAGE_HEADER_OFFSET);
			STAT_ADD_IMPLEMENTATION(-sizeof(regis1_page_header));

			BIBOP_FREE_PAGE(s_p.bop, page);

		} else {
			/* Freeing bump page. Reset bump region, don't free page. */

			STAT_ADD_INTERNAL((ptrdiff_t) page - (ptrdiff_t) REGION_GET_CUR(s_p._bump));
			REGION_SET_CUR(s_p._bump, page);

		}

	}

}


void regis1_cleanup() {

	/* Do nothing. */

}
