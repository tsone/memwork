#---------------------------------------------------------------------------------
# NDS configuration
#---------------------------------------------------------------------------------

ifeq ($(strip $(DEVKITARM)),)
$(error "Please set DEVKITARM in your environment. export DEVKITARM=<path to>devkitARM")
endif
include $(DEVKITARM)/ds_rules

ARCH		:= -mthumb -mthumb-interwork

ASFLAGS		+= $(ARCH)
CFLAGS		+= $(ARCH) -DARM9 -march=armv5te -mtune=arm946e-s
LDFLAGS		+= $(ARCH) -specs=ds_arm9.specs -Wl,-Map,$(notdir $*.map)

ifeq ($(BUILD),release)
LIBS		+= -lfat -lnds9
else
LIBS		+= -lfatd -lnds9d
endif

LIBDIRS		+= $(BASEDIR)/../libnds-1.5.7-mod $(BASEDIR)/../libfat-1.0.11-mod

TARGET		:= $(TARGET).nds