#!/bin/bash
echo "NOTE: Run this script with superuser privileges on a single-core configuration."
echo "Setting real-time..."
echo -1 > /proc/sys/kernel/sched_rt_runtime_us
echo "Setting 'performance' governor..."
echo performance > /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
echo "Disabling daemons..."
chkconfig ebtables off
chkconfig livesys off        	
chkconfig livesys-late off
chkconfig netconsole off
chkconfig network off
chkconfig spice-vdagentd off
systemctl stop dbus
systemctl stop firewalld
#systemctl stop udev
systemctl stop abrt-xorg
systemctl stop abrtd
systemctl stop atd               
systemctl stop auditd
systemctl stop avahi-daemon
systemctl stop crond             
systemctl stop mcelog
systemctl stop wpa_supplicant
systemctl stop NetworkManager
systemctl stop nfs-lock
systemctl stop rpcbind
systemctl stop rsyslog
systemctl stop syslog             
systemctl stop smartd
systemctl stop systemd-logind
systemctl stop systemd-journald  
#systemctl stop udev-control
#systemctl stop udev-kernel        
echo "Running memwork..."
./linux/release/memwork > /dev/null
echo "DONE!"
chown -R tsone:tsone .

