/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#define INTERNAL_H_
#ifndef _STDDEF_H
#include <stddef.h>
#endif
#ifndef _STDIO_H
#include <stdio.h>
#endif
#ifndef _STDLIB_H
#include <stdlib.h>
#endif
#ifndef ARM9
#ifndef _ASSERT_H
#include <assert.h>
#endif
#endif


#ifdef ARM9

#ifndef NDS_INCLUDE
#include <nds.h>
#endif


#elif W32

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

/* Works at least for IA32. */
typedef char i8;
typedef short i16;
typedef int i32;
typedef long long i64;
typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef signed char s8;
typedef signed short s16;
typedef signed int s32;
typedef signed long long s64;

#elif LINUX

#ifndef _LINUX_TYPES_H
#include <linux/types.h>
#endif

typedef __u8 u8;
typedef __u16 u16;
typedef __u32 u32;
typedef __u64 u64;
typedef __s8 s8;
typedef __s16 s16;
typedef __s32 s32;
typedef __s64 s64;

#endif


#define OFFSETOF(type_, member_)        ((size_t) &((type_ *) 0)->member_)

#define SIZE(shift_)                    (1 << (shift_))
#define MASK(shift_)                    (SIZE(shift_) - 1)
#define ALIGN_DOWN(num_, shift_)        (((size_t) (num_)) & (~MASK(shift_)))
#define ALIGN_UP(num_, shift_)          ALIGN_DOWN((num_) + MASK(shift_), (shift_))


#define MIN_MALLOC			1	/* 1 B */
#define MAX_MALLOC_SH                   9       /* 512 B */
#define MAX_MALLOC 			SIZE(MAX_MALLOC_SH)
#define QUANTUM_SH			3	/* 8 B */

#define NUM_CLASSES			(MAX_MALLOC / SIZE(QUANTUM_SH))

#define ALIGN_UP_TO_CLASS(size_bytes_) \
		ALIGN_UP((size_bytes_), QUANTUM_SH)


#define SIZE_TO_CLASS_IDX(size_bytes_) \
		(((size_bytes_) - 1) >> QUANTUM_SH)


#define CLASS_IDX_TO_SIZE(class_idx_) \
		(((class_idx_) + 1) << QUANTUM_SH)


#ifdef ARM9
#define PRINT(...)	do { \
		iprintf(__VA_ARGS__); \
} while (0)
#else
#define PRINT(...)	do { \
		printf(__VA_ARGS__); \
} while (0)
#endif

#ifndef NDEBUG

#ifdef ARM9

#define ASSERT(a_) do { \
		sassert((a_), #a_); \
} while (0)
#else


#define ASSERT(a_) do { \
		if (!(a_)) { \
			PRINT("ASSERT:" __FILE__ ":%d:%s\nASSERT:EXITING...", __LINE__, #a_); \
			fflush(stdout); \
			while(1) ; \
		} \
} while (0)

#endif


#define PDEBUG(...)	PRINT("DEBUG:" __VA_ARGS__)
#define PERROR(...)	PRINT("ERROR:" __VA_ARGS__)


#else


#define ASSERT(a_)
#define PDEBUG(...)
#define PERROR(...)	PRINT("ERROR:" __VA_ARGS__)


#endif

