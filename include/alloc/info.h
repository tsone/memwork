/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#define INFO_H_
#ifndef INTERNAL_H_
#include "internal.h"
#endif


#ifndef INFO_DISABLE

#ifndef NDEBUG
#define INFO_DISABLE (0)
#else
#define INFO_DISABLE (1)
#endif

#endif


#if (INFO_DISABLE == 0)


#define INFO(prefix_, name_) prefix_ info name_;


#define INFO_INIT(p_, start_address_, implementation_) do { \
		(p_)._allocated = 0; \
		(p_)._internal = 0; \
		(p_)._implementation = (implementation_); \
		/* NOTE: This was just a quick hack to include implementation
		 * overhead to external fragmentation calculation
		 */ \
		(p_)._min_address = (void*) (((size_t) (start_address_)) - (implementation_)); \
		(p_)._max_address = (start_address_); \
} while (0)


#define INFO_COPY(p_, to_) do { \
		(to_)._allocated = (p_)._allocated; \
		(to_)._internal = (p_)._internal; \
		(to_)._implementation = (p_)._implementation; \
		(to_)._min_address = (p_)._min_address; \
		(to_)._max_address = (p_)._max_address; \
} while (0)


#define INFO_ADD_ALLOCATED(p_, size_bytes_) do { \
		(p_)._allocated += (size_bytes_); \
} while (0)


#define INFO_ADD_INTERNAL(p_, site_bytes_) do { \
		(p_)._internal += (site_bytes_); \
} while (0)


#define INFO_ADD_IMPLEMENTATION(p_, size_bytes_) do { \
		(p_)._implementation += (size_bytes_); \
} while (0)


#define INFO_MAX_ADDRESS(p_, ptr_max_) do { \
		ASSERT((ptr_max_) >= (p_)._min_address); \
		if ((ptr_max_) > (p_)._max_address) { \
			(p_)._max_address = (ptr_max_); \
		} \
} while (0)


/* TODO: should the header operations be moved elsewhere? */
#define INFO_HDR_MAGIC ((unsigned short) 0xFECE)


#define INFO_HDR_GET_IDX(ptr_, idx_) \
		(((unsigned short*) (ptr_))[(idx_)])


#define INFO_HDR_GET_MAGIC(ptr_) \
		INFO_HDR_GET_IDX((ptr_), 0)


#define INFO_HDR_GET_ALLOCATED(ptr_) \
		INFO_HDR_GET_IDX((ptr_), 1)


#define INFO_HDR_GET_INTERNAL(ptr_) \
		INFO_HDR_GET_IDX((ptr_), 2)


#define INFO_HDR_GET_IMPLEMENTATION(ptr_) \
		INFO_HDR_GET_IDX((ptr_), 3)


#define INFO_HDR_ASSERT(ptr_) do { \
		ASSERT(INFO_HDR_GET_MAGIC(ptr_) == INFO_HDR_MAGIC); \
} while (0)


#define INFO_HDR_INIT(ptr_, allocated_, internal_, implementation_) do { \
		INFO_HDR_GET_MAGIC(ptr_) = INFO_HDR_MAGIC; \
		INFO_HDR_GET_ALLOCATED(ptr_) = (unsigned short) (allocated_); \
		INFO_HDR_GET_INTERNAL(ptr_) = (unsigned short) (internal_); \
		INFO_HDR_GET_IMPLEMENTATION(ptr_) = (unsigned short) (implementation_); \
} while (0)


#define INFO_HDR_CLEANUP(ptr_) do { \
		INFO_HDR_GET_MAGIC(ptr_) = (unsigned short) (~INFO_HDR_MAGIC); \
} while (0)


typedef struct t_info {

	/**
	 * Allocated memory in bytes.
	 */
	size_t _allocated;

	/**
	 * Internal fragmentation bytes.
	 */
	size_t _internal;

	/**
	 * Implementation bookkeeping and data structures bytes.
	 */
	size_t _implementation;

	/**
	 * Minimum address reached (allocated) during run.
	 */
	void	*_min_address;

	/**
	 * Maximum address reached (allocated) during run.
	 */
	void	*_max_address;

} info;


#elif (INFO_DISABLE == 1)


#define INFO(prefix_, name_)
#define	INFO_INIT(p_, start_address_, implementation_)
#define INFO_COPY(p_, to_)
#define INFO_ADD_ALLOCATED(p_, size_bytes_)
#define INFO_ADD_INTERNAL(p_, size_bytes_)
#define INFO_ADD_IMPLEMENTATION(p_, size_bytes_)
#define INFO_MAX_ADDRESS(p_, ptr_max_)

#define INFO_HDR_GET_MAGIC(ptr_)
#define INFO_HDR_GET_ALLOCATED(ptr_)
#define INFO_HDR_GET_INTERNAL(ptr_)
#define INFO_HDR_GET_IMPLEMENTATION(ptr_)
#define INFO_HDR_ASSERT(ptr_)
#define INFO_HDR_INIT(ptr_, allocated_, internal_, implementation_)
#define INFO_HDR_CLEANUP(ptr_)


#else


#error "INFO_DISABLE configuration error."


#endif

