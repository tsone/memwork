/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
/**
 * A circular doubly linked list data structure implementation (in ANSI C).
 *
 * Most of the defines take input as immediate instead of pointer.
 * This is to discourage the use of pointers.
 *
 * NOTE: Include this header with header guards:
 *
 *     #ifndef CDL_H_
 *     #include "cdl.h"
 *     #endif
 *
 */
#define CDL_H_
#ifndef INTERNAL_H_
#include "internal.h"
#endif


#define CDL_GET_BASE(p_, type_, member_) \
		((type_ *) ((size_t) (&(p_)) - OFFSETOF(type_, member_)))


typedef struct t_cdl_node {

	struct t_cdl_node *_prev;
	struct t_cdl_node *_next;

} cdl_node;


/**
 * Obtain pointer to previous item in list.
 *
 */
#define CDL_PREV(p_) ((p_)._prev)


/**
 * Obtain pointer to next item in list.
 *
 */
#define CDL_NEXT(p_) ((p_)._next)


/**
 * Check if item is the sole item in a list.
 *
 */
#define CDL_IS_ORPHAN(p_) \
		(CDL_NEXT(p_) == CDL_NEXT(*CDL_NEXT(p_)))


/**
 * Check if node points to itself. Faster than CDL_IS_ORPHAN().
 * WARNING! Use this only if you know what you are doing!
 *
 */
#define CDL_IS_ORPHAN_UNSAFE(p_) \
                (&(p_) == CDL_PREV(p_))


/**
 * Initialize a list link. Do this before other operations to the list.
 *
 */
#define CDL_INIT(p_) do { \
		CDL_PREV(p_) = &(p_); \
		CDL_NEXT(p_) = &(p_); \
} while (0)


/**
 * Insert item after this in list.
 *
 */
#define CDL_INSERT_AFTER(p_, after_) do { \
		CDL_PREV(after_) = &(p_); \
		CDL_NEXT(after_) = CDL_NEXT(p_); \
		CDL_PREV(*CDL_NEXT(p_)) = &(after_); \
		CDL_NEXT(p_) = &(after_); \
} while (0)


#define CDL_INSERT_BEFORE(p_, before_) do { \
                CDL_PREV(before_) = CDL_PREV(p_); \
                CDL_NEXT(before_) = &(p_); \
                CDL_NEXT(*CDL_PREV(p_)) = &(before_); \
                CDL_PREV(p_) = &(before_); \
} while (0)


/**
 * Remove this item from a list without initializing removed list links.
 *
 */
#define CDL_REMOVE_UNSAFE(p_) do { \
		CDL_NEXT(*CDL_PREV(p_)) = CDL_NEXT(p_); \
		CDL_PREV(*CDL_NEXT(p_)) = CDL_PREV(p_); \
} while (0)


/**
 * Remove this time from a list.
 *
 */
#define CDL_REMOVE(p_) do { \
		CDL_REMOVE_UNSAFE(p_); \
		CDL_INIT(p_); \
} while (0)
