/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
/**
 * Reap allocator with BIBOP paging and size classes.
 *
 * Allocations are done from bump region pages corresponding to the size class
 * (i.e. regis2_class _classes).
 *
 */
#define REAPS_H_
#ifndef INTERNAL_H_
#include "internal.h"
#endif


size_t	reaps_init(void *start, size_t size_bytes);
void*	reaps_malloc(size_t size_bytes);
void 	reaps_free(void *ptr);
void	reaps_cleanup();


