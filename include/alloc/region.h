/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#define REGION_H_


#define REGION_GET_CUR(p_)	((p_)._cur)


#define REGION_GET_END(p_)	((p_)._end)


#define REGION_SET_CUR(p_, cur_) do { \
		REGION_GET_CUR(p_) = (cur_); \
} while (0)


#define REGION_SET_END(p_, end_) do { \
		REGION_GET_END(p_) = (end_); \
} while (0)


#define REGION_INIT(p_, start_, end_) do { \
		REGION_SET_CUR((p_), (start_)); \
		REGION_SET_END((p_), (end_)); \
} while (0)


#define REGION_IS_FULL(p_) \
		(REGION_GET_CUR(p_) >= REGION_GET_END(p_))


#define REGION_CAN_ALLOCATE(p_, size_bytes_) \
		((void*) ((size_t) REGION_GET_CUR(p_) + (size_bytes_)) <= REGION_GET_END(p_))


/**
 * Get remaining capacity in bytes.
 */
#define REGION_GET_REMAINING(p_) \
	((size_t) REGION_GET_END(p_) - (size_t) REGION_GET_CUR(p_))


/**
 * Allocate bytes from region.
 *
 * \note There is no safe version. Use REGION_ALLOC_FAILED to check if
 * allocation failed.
 *
 */
#define REGION_ALLOC_UNSAFE(p_, size_bytes_, result_) do { \
		(result_) = REGION_GET_CUR(p_); \
		REGION_SET_CUR((p_), (void*) ((size_t) REGION_GET_CUR(p_) + (size_bytes_))); \
} while (0)


/**
 * Check if previous REGION_ALLOC_UNSAFE failed.
 *
 * \return True if previous REGION_ALLOC_UNSAFE failed, false otherwise.
 *
 */
#define REGION_ALLOC_FAILED(p_) \
		(REGION_GET_CUR(p_) > REGION_GET_END(p_))


typedef struct t_region {

	void *_cur;
	void *_end;

} region;

