/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
/**
 * Reap implementation.
 *
 * Allocates blocks in constant-time. Has no memory overhead (except struct).
 * Free blocks are stored in a single-linked list which  pointers are
 * contained in the free blocks. Block sizes are not managed.
 *
 */
#define REAP_H_
#ifndef LIST_H_
#include "list.h"
#endif
#ifndef REGION_H_
#include "region.h"
#endif


#define REAP_INIT(p_, start_, end_) do { \
		LIST_INIT((p_)._free); \
		REGION_INIT((p_)._region, (start_), (end_)); \
} while (0)


#define REAP_GET_END(p_) REGION_GET_END((p_)._region)


/*
#define REAP_IS_FULL(p_) \
	(LIST_IS_EMPTY((p_)._free) && REGION_IS_FULL((p_)._region))
*/


#define REAP_CAN_ALLOCATE(p_, size_bytes_) \
		(LIST_HAS_ITEMS((p_)._free) || REGION_CAN_ALLOCATE((p_)._region, (size_bytes_)))


#define REAP_ALLOC_WITH_RETURN(p_, size_bytes_, result_) do { \
		if (LIST_HAS_ITEMS((p_)._free)) { \
			LIST_POP((p_)._free, (result_)); \
		} else if (!REGION_IS_FULL((p_)._region)) { \
			REGION_ALLOC_UNSAFE((p_)._region, (size_bytes_), (result_)); \
		} else { \
			return 0; \
		} \
} while (0)


#define REAP_ALLOC_UNSAFE(p_, size_bytes_, result_) do { \
		if (LIST_HAS_ITEMS((p_)._free)) { \
			LIST_POP((p_)._free, (result_)); \
		} else { \
			REGION_ALLOC_UNSAFE((p_)._region, (size_bytes_), (result_)); \
		} \
} while (0)


#define REAP_FREE(p_, ptr_) do { \
		LIST_PUSH((p_)._free, (ptr_)); \
} while (0)


/*
#define REAP_INFO(p_, i_) \
		(i_)._capacity = (p_)._region._i._capacity; \
		(i_)._free = (p_)._region._i._free + (p_)._free._i._free; \
		(i_)._implementation = sizeof(reap);
*/


typedef struct t_reap {

	list _free;
	region _region;

} reap;

