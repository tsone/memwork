/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#define STAT_H_
#ifndef INFO_H_
#include "info.h"
#endif


#define STAT_INFO g_stat


#define STAT_INIT(start_, implementation_) \
		INFO_INIT(STAT_INFO, (start_), (implementation_))


#define STAT_COPY(to_) \
		INFO_COPY(STAT_INFO, (to_))


#define STAT_ADD_ALLOCATED(size_bytes_) \
		INFO_ADD_ALLOCATED(STAT_INFO, (size_bytes_))


#define STAT_ADD_INTERNAL(size_bytes_) \
		INFO_ADD_INTERNAL(STAT_INFO, (size_bytes_))


#define STAT_ADD_IMPLEMENTATION(size_bytes_) \
		INFO_ADD_IMPLEMENTATION(STAT_INFO, (size_bytes_))


#define STAT_HDR_INIT(ptr_, allocated_, internal_, implementation_) do { \
                /* NOTE: Implementation overhead must not be added! */ \
		INFO_MAX_ADDRESS(STAT_INFO, (void*) (((size_t) (ptr_)) \
				+ (allocated_) + (internal_) /*+ (implementation_)*/)); \
		INFO_HDR_INIT((ptr_), (allocated_), (internal_), (implementation_)); \
} while (0)

INFO(extern, STAT_INFO)

