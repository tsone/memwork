/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
/**
 * BIBOP implementation.
 *
 * Manages address-aligned pages of SIZE(BIBOP_PAGE_SH) bytes. When NDEBUG
 * is not set, tags freed blocks with BIBOP_PAGE_FREED_MAGIC.
 * Use BIBOP_ASSERT* macros for debug assertions.
 *
 */
#define BIBOP_H_
#ifndef REAP_H_
#include "reap.h"
#endif
#ifndef INTERNAL_H_
#include "internal.h"
#endif


/* CONFIG: Please set this before including or uncomment here. */
//#define BIBOP_PAGE_SH				12	/* 4 KB */

#define BIBOP_PAGE_FREED_MAGIC		0xA55B1B0B


#ifndef NDEBUG


#define BIBOP_SET_PAGE_FREED(page_) do { \
		BIBOP_GET_PAGE_FREED(page_) = BIBOP_PAGE_FREED_MAGIC; \
} while (0)


#else


#define BIBOP_SET_PAGE_FREED(page_)


#endif


#define BIBOP_PAGE_HEADER_OFFSET(header_type_) \
		(SIZE(BIBOP_PAGE_SH) - sizeof(header_type_))


#define BIBOP_PAGE_HEADER(page_, header_type_) \
		((header_type_*) ((size_t) (page_) + BIBOP_PAGE_HEADER_OFFSET(header_type_)))


#define BIBOP_GET_PAGE_FREED(page_) \
		(*BIBOP_PAGE_HEADER((page_), size_t))


#define BIBOP_GET_PAGE_FROM_PTR(p_, ptr_) \
		ALIGN_DOWN((ptr_), BIBOP_PAGE_SH)


#define BIBOP_ASSERT_VALID_PTR(p_, ptr_) do { \
		ASSERT(((ptr_) >= BIBOP_GET_START(p_)) && ((ptr_) < BIBOP_GET_END(p_))); \
} while (0)


#define BIBOP_ASSERT_PAGE_NOT_FREED(p_, page_) do { \
		ASSERT(BIBOP_GET_PAGE_FREED(page_) != BIBOP_PAGE_FREED_MAGIC); \
} while (0)


#define BIBOP_GET_START(p_) \
		((p_)._start)


#define BIBOP_GET_END(p_) \
		REAP_GET_END((p_)._pages)


#define BIBOP_GET_CAPACITY(p_) \
		((size_t) REAP_GET_END((p_)._pages) - (size_t) BIBOP_GET_START(p_))


#define BIBOP_INIT(p_, start_, size_bytes_) do { \
		ASSERT((start_) && ((size_bytes_) > 0)); \
		BIBOP_GET_START(p_) = (void*) ALIGN_UP((size_t) (start_), BIBOP_PAGE_SH); \
		REAP_INIT((p_)._pages, BIBOP_GET_START(p_), \
				(void*) BIBOP_GET_PAGE_FROM_PTR((p_), ((size_t) (start_)) + (size_bytes_))); \
} while (0)


#define BIBOP_ALLOC_PAGE(p_, result_) do { \
		REAP_ALLOC_WITH_RETURN((p_)._pages, SIZE(BIBOP_PAGE_SH), (result_)); \
		INFO_MAX_ADDRESS(STAT_INFO, \
				(void*) (((size_t) (result_)) + SIZE(BIBOP_PAGE_SH))); \
} while (0)


#define BIBOP_FREE_PAGE(p_, page_) do { \
		REAP_FREE((p_)._pages, (page_)); \
		BIBOP_SET_PAGE_FREED(page_); \
} while (0)


typedef struct t_bibop {

	reap _pages;
	void *_start;

} bibop;

