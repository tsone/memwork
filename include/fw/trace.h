/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#define TRACE_H_
#ifndef INTERNAL_H_
#include "internal.h"
#endif
#ifndef FW_H_
#include "fw.h"
#endif


#define TRACE_FREE_MAGIC        0xFFFF


typedef struct t_trace_op {

        size_t  slot;
        size_t  size;

} trace_op;


size_t	trace_init(fw_generator *generator, const char *in_file);
void	trace_cleanup();
/** Get next memory op from trace. Return 0 when no ops left in trace. */
size_t  trace_next(trace_op *op);
void	trace_rewind();

