/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#define CACHEF_H_
#ifndef INTERNAL_H_
#include "internal.h"
#endif
#ifndef _STDIO_H_
#include <stdio.h>
#endif


typedef enum t_cachef_mode {

	CACHEF_MODE_WRITE = 1,
	CACHEF_MODE_READ

} cachef_mode;


typedef struct t_cachef {

	FILE		*_f;
	void		*_cache;
	char		*_filename;
	size_t		_capacity;
	size_t		_idx;
	cachef_mode	_mode;

} cachef;


size_t	cachef_init(cachef *p, size_t capacity, const char *filename, cachef_mode mode);
void	cachef_cleanup(cachef *p);
void	cachef_flush(cachef *p);
void	cachef_write(cachef *p, const void *data, size_t num_bytes);
void	cachef_write_vlq(cachef *p, size_t number);
void	cachef_write_vlq_signed(cachef *p, ptrdiff_t number);
