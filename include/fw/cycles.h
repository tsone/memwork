/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#define CYCLES_H_
#ifndef INTERNAL_H_
#include "internal.h"
#endif


#ifdef NDEBUG


#ifdef ARM9


#define CYCLES_INIT() do { \
	    TIMER_CR(3) = 0; \
	    TIMER_CR(2) = 0; \
	    TIMER_CR(1) = 0; \
	    TIMER_CR(0) = 0; \
	    TIMER_DATA(0) = 0; \
	    TIMER_DATA(1) = 0; \
	    TIMER_DATA(2) = 0; \
	    TIMER_DATA(3) = 0; \
		TIMER_CR(3) = TIMER_CASCADE | TIMER_ENABLE; \
	    TIMER_CR(1) = TIMER_CASCADE | TIMER_ENABLE; \
		TIMER_CR(2) = TIMER_ENABLE; \
	    TIMER_CR(0) = TIMER_ENABLE; \
} while (0)


#define CYCLES_RESTART() do { \
		TIMER_CR(3) = 0; \
		TIMER_CR(2) = 0; \
		TIMER_CR(1) = 0; \
	    TIMER_CR(0) = 0; \
		TIMER_CR(3) = TIMER_CASCADE | TIMER_ENABLE; \
	    TIMER_CR(1) = TIMER_CASCADE | TIMER_ENABLE; \
		TIMER_CR(2) = TIMER_ENABLE; \
	    TIMER_CR(0) = TIMER_ENABLE; \
} while (0)


#define CYCLES_GET(result_) do { \
		(result_) = ((TIMER_DATA(0) | (TIMER_DATA(1) << 16)) \
			+ (TIMER_DATA(2) | (TIMER_DATA(3) << 16))) >> 1; \
} while (0)



#else


extern u64 g_tick_start;
extern u64 g_tick_stop;


static __inline__ u64 cycles_start() {
	u32 lo, hi;

	__asm__ __volatile__ (
#if (LINUX && NDEBUG)
                /* WARNING! Disable interrupts. */
                "cli\n\t"
#endif
		"xor %%eax, %%eax\n\t"
		"cpuid\n\t"
		"rdtsc\n\t"
		: "=d"(hi), "=a"(lo)
		: 
		: "%ebx", "%ecx"
	);

	return ((u64) lo) | (((u64) hi) << 32);
}


static __inline__ u64 cycles_stop() {
	u32 lo, hi;

#if 0
        /* NOTE: Atom. Measurement error is 1 instruction. */
	__asm__ __volatile__ (
		"rdtsc\n\t"
#if (LINUX && NDEBUG)
                /* WARNING! Enable interrupts. */
                "sti\n\t"
#endif
		: "=d"(hi), "=a"(lo)
                :
		: 
	);
#endif 

#if 1
        /* NOTE: Atom. Measurement error is 1 instruction. Prevent execution ordering. */
	__asm__ __volatile__ (
		"rdtsc\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t"
		"mov $0, %%eax\n\t"
		"cpuid\n\t"
#if (LINUX && NDEBUG)
                /* WARNING! Enable interrupts. */
                "sti\n\t"
#endif
		: "=r"(hi), "=r"(lo)
                :
		: "%eax", "%ebx", "%ecx", "%edx"
	);
#endif

#if 0
        /* NOTE: This cr0 synch method needs 0 privilege mode (kernel). */
	__asm__ __volatile__ (
		"mov %%cr0, %%eax\n\t"
		"mov %%eax, %%cr0\n\t"
		"rdtsc\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t"
		"mov $0, %%eax\n\t"
		"cpuid\n\t"
#if (LINUX && NDEBUG)
                /* WARNING! Enable interrupts. */
                "sti\n\t"
#endif
		: "=r"(hi), "=r"(lo)
                :
		: "%eax", "%ebx", "%ecx", "%edx"
	);
#endif

#if 0
        /* NOTE: Only works if RDTSCP is supported (not in Atom N270). */
	__asm__ __volatile__ (
		"rdtscp\n\t"
		"mov %%edx, %0\n\t"
		"mov %%eax, %1\n\t"
		"mov $0, %%eax\n\t"
		"cpuid\n\t"
#if (LINUX && NDEBUG)
                /* WARNING! Enable interrupts. */
                "sti\n\t"
#endif
		: "=r"(hi), "=r"(lo)
                :
		: "%eax", "%ebx", "%ecx", "%edx"
	);
#endif

	return ((u64) lo) | (((u64) hi) << 32);
}

#define CYCLES_INIT()


#define CYCLES_RESTART() do { \
		g_tick_start = cycles_start(); \
} while (0)


#define CYCLES_GET(result_) do { \
		g_tick_stop = cycles_stop(); \
		(result_) = (g_tick_stop - g_tick_start); \
} while (0)


#endif


#else

#define CYCLES_INIT()
#define CYCLES_RESTART() 
#define CYCLES_GET(result_)

#endif

