/*
 * Copyright (c) 2012, Valtteri Heikkil�
 *
 * Permission to use, copy, modify, and/or distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN
 * AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT
 * OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 *
 */
#define FW_H_
#ifndef INTERNAL_H_
#include "internal.h"
#endif
#ifndef INFO_H_
#include "info.h"
#endif


// TODO: Remove and check if this is valid.
// CONFIG: Undefine if use new type of trace files.
#define FW_TRACE_ORIG

#define FW_ITERATIONS	1
#if (INFO_DISABLE == 1)
#ifndef ARM9
#undef FW_ITERATIONS
#define FW_ITERATIONS	1000
#endif
#endif

#define FW_TEST_HEAP_SIZE	(3*1024*1024)	// 3 MB
#define FW_TEST_PAGE_SIZE	(4*1024)		// 4 KB

#define FW_TEST_MIN_MALLOC	(1)				// 1 B
#define FW_TEST_MAX_MALLOC	(512)			// 512 B


//typedef struct t_generator_item {
//
//	word size;
//	word idx;
//
//} generator_item;


typedef size_t	(*f_fw_init)(void *heap_start, size_t heap_size);
typedef void*	(*f_fw_malloc)(size_t size);
typedef void	(*f_fw_free)(void *ptr);
typedef void	(*f_fw_cleanup)(void *heap_start);


typedef struct t_fw_allocator {

	const char		*name;
	f_fw_init		f_init;
	f_fw_malloc		f_malloc;
	f_fw_free		f_free;
	f_fw_cleanup	f_cleanup;

} fw_allocator;


typedef struct t_fw_generator {

        size_t num_slots;
	size_t num_ops;

} fw_generator;


size_t	fw_init(fw_allocator *allocator, fw_generator *gen, const char *output_path);
size_t	fw_run(void *heap_start, size_t heap_size);
void	fw_cleanup();

void	fw_free_slot(size_t slot_idx);
size_t	fw_malloc_slot(size_t slot_idx, size_t size_bytes);

